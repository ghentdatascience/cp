# config with:
# export GECODE_HOME=.../gecode-4.4.0
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GECODE_HOME

BASIC = common/fmrdb_basic.o common/constraint_coverglobal.o common/relation.o common/reader_nrminer.o
OBJECTS = \
	rel_closed \
	rel_maxent

# Compiler stuff
CXXFLAGS = \
		-I. -I${GECODE_HOME} \
		-DNDEBUG -fvisibility=hidden -fPIC -Wextra -Wall -Wno-terminate -pipe \
		-ggdb -O3 -fno-strict-aliasing -finline-limit=3000 \
		-ffast-math
LDFLAGS = \
		-L. -L${GECODE_HOME}
LDLIBS = \
		-lpthread \
		-lgecodekernel -lgecodedriver \
		-lgecodesearch -lgecodeint \
		-lgecodesupport -lgecodefloat \
		-lgecodegist # uncomment if you compiled gecode with GIST support
		              # (change it, if you get GIST related compile errors)

all: ${BASIC} $(OBJECTS) values

${BASIC}: common/fmrdb_basic.hh common/relation.hh common/reader_nrminer.hh common/options_maxent.cpp

$(OBJECTS): ${BASIC}

values:
	cd get_values; $(MAKE) $(MFLAGS)

rel_maxent: ${BASIC} constraint_maxent.o

clean: 
	rm -f *.o ${BASIC} $(OBJECTS) get_values/get_values

dist-clean: clean

