/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *      Achille Aknin <achille.aknin@ens.fr>
 *
 *  Some code based N-RMiner's CSVReader.cpp (by Eirini Spyropoulou)
 * 
 *  Copyright:
 *      all authors
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __FIMCP_COMMON_READERNRMINER__
#define __FIMCP_COMMON_READERNRMINER__

#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <cstring>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include "DatabaseGenerator.h"
//#include <common/fmrdb_basic.hh>

// http://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c
inline bool ends_with(std::string const & value, std::string const & ending)
{
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

using namespace std;

/**
 * Class to read dir with N-RMiner files
 */
class Reader_NRMiner {
public:
  vector<pair<vector<int>,vector<int> > > sums;
  vector <map <string, int> > correspondence;
  vector<pair<int, int> > ids;
  map<string, int> ent_ids;
  
  
  Reader_NRMiner() : sums(), ids(), ent_ids() { };
  ~Reader_NRMiner() { };
  
  virtual void read (const char* filename1);
  virtual void print();

protected:
  vector<string> list_dir(const char* dirname);
  void read_relfile(int r, const string& relfile);
  void write(int r, const string& relfile, NullModel* model);
};


void Reader_NRMiner::read (const char* dirname) {
  const vector<string> relfiles = list_dir(dirname);
  for (size_t r=0; r!=relfiles.size(); r++)
    read_relfile(r, relfiles[r]);
  
  int nr_ent = ent_ids.size();
  vector<int> sizes(nr_ent, 0);
  for (size_t r=0; r!=sums.size(); r++) { // collect
    if (sums[r].first.size() > sizes[ids[r].first])
      sizes[ids[r].first] = sums[r].first.size();
    if (sums[r].second.size() > sizes[ids[r].second])
      sizes[ids[r].second] = sums[r].second.size();
  }
  for (size_t r=0; r!=sums.size(); r++) { // collect
    for (size_t i = sums[r].first.size(); i != sizes[ids[r].first]; i++)
      sums[r].first.push_back(0);
    for (size_t i = sums[r].second.size(); i != sizes[ids[r].second]; i++)
      sums[r].second.push_back(0);
  }

  
  for (size_t r = 0; r!=sums.size(); r++) {
    NullModel model(&(sums[r].first), &(sums[r].second));

    write(r, relfiles[r], &model);
  }
  //print();
}

vector<string> Reader_NRMiner::list_dir(const char* dirname)
{
  printf("Reading relations from '%s'\n", dirname);
  DIR* dp;
  dirent* dirp;
  vector<string> relfiles;
  
  if((dp  = opendir(dirname)) == NULL) {
    cout << "Error(" << errno << ") nary opening directory: " <<dirname << endl;
    return relfiles;
  }
  
  while ((dirp = readdir(dp)) != NULL) {
    if ((ends_with(dirp->d_name, ".txt") && !ends_with(dirp->d_name, "_values.txt"))
        || ends_with(dirp->d_name, ".csv")) {
      string filename = string(dirname)+"/"+string(dirp->d_name);
      relfiles.push_back(filename);
    }
  }
  closedir(dp);
  
  return relfiles;
}

void Reader_NRMiner::read_relfile(int r, const string& relfile)
{
  cout << "Reading:"<<relfile<<"\n";
  // read header
  string header;
  fstream narystream(relfile.c_str(),ios::in);
  getline(narystream, header);
  if (header.find('\r')!=string::npos || header.find('\n')!=string::npos)
    header.erase(header.length()-1);
  
  stringstream line_stream(header);
  string entname;
  int entid[2] = {0,0};
  int i=0;
  while (getline(line_stream, entname, ',')) {
    if (i > 1) cout << "Error, too many entities while reading file\n";
    if (ent_ids.count(entname) == 0) {
      int id=(int)ent_ids.size();
      ent_ids[entname] = id;
      map<string,int> corr_id;
      correspondence.push_back(corr_id);
    }
    entid[i++] = ent_ids[entname];
  }
    
  ids.push_back(make_pair(entid[0], entid[1]));
  vector<int> rowSums; 
  vector<int> colSums; 
  
  
  // read in rest
  string line;
  getline(narystream, line);
    // read edges
    string node_str;
    int max1 = 0;
    int max2 = 0;
    do {
      if (line.find("\r") != line.npos || line.find("\n") != line.npos)
	line.erase(line.length()-1);
      
      stringstream line_stream(line);
      int node1;
      int node2;
      string node1_file;
      string node2_file;
      getline(line_stream, node1_file, ',');
      getline(line_stream, node2_file, ',');
      
      /*getline(line_stream, node_str, ',');
      node1 = atoi(node_str.c_str());
      getline(line_stream, node_str, ',');
      node2 = atoi(node_str.c_str());*/
      
      if (correspondence[entid[0]].count(node1_file) == 0) {
        int id=(int)correspondence[entid[0]].size();
        correspondence[entid[0]][node1_file] = id+1;
      }
      node1 = correspondence[entid[0]][node1_file];

      if (correspondence[entid[1]].count(node2_file) == 0) {
        int id=(int)correspondence[entid[1]].size();
        correspondence[entid[1]][node2_file] = id+1;
      }
      node2 = correspondence[entid[1]][node2_file];


      if (node1 == 0 || node2 == 0)
        cout << "ERROR: data assumed to be offset 1, but found node with id '0'!!\n";
      if (max1 < node1) {
	for (int i = max1; i < node1; i++)
	  rowSums.push_back(0);
        max1 = node1;
      }
      if (max2 < node2) {
	for (int i = max2; i < node2; i++)
	  colSums.push_back(0);
        max2 = node2;
      }
      rowSums[node1-1]++;
      colSums[node2-1]++;
    } while (getline(narystream, line));
    sums.push_back(make_pair(rowSums,colSums));
  narystream.close();
}

void Reader_NRMiner::write(int r, const string& relfile, NullModel* model) {
  string header;
  fstream narystream(relfile.c_str(),ios::in);
  getline(narystream, header);


 
  string outputfile = relfile.substr(0, relfile.length() - 4) + "_values.txt";
  cout << "Writing:"<<outputfile<<"\n";
  fstream outputstream(outputfile.c_str(), ios_base::out|ios_base::trunc);

  outputstream << header << endl;

  string line;
  string node_str;
  getline(narystream, line);
  do {
    if (line.find("\r") != line.npos || line.find("\n") != line.npos)
      line.erase(line.length()-1);

    stringstream line_stream(line);
    int node1;
    string node1_file;
    int node2;
    string node2_file;
    getline(line_stream, node1_file, ',');
    getline(line_stream, node2_file, ',');

    pair <int,int> entid = ids[r];

    node1 = correspondence[entid.first][node1_file];
    node2 = correspondence[entid.second][node2_file];

    
    outputstream << -model->logProbability(node1-1, node2-1) << endl;
  } while (getline(narystream, line));

  narystream.close();
  outputstream.close();
}

void Reader_NRMiner::print()
{
  /*cout << "Relation 1 between ent " << ids[1].first << " and " << ids[1].second << " :" << endl;

  cout << "RowSum : [";
  for (int i = 0; i < sums[1].first.size() ; i++) {
    cout << sums[1].first[i] << ";";
  }
  cout << "]" << endl;

  cout << "ColSum : [";
  for (int i = 0; i < sums[1].second.size() ; i++) {
    cout << sums[1].second[i] << ";";
  }
  cout << "]" << endl;*/
}

int main (int argc, char *argv[]) {
  if (argc == 1) {
    cout << "Error : Expected a folder as argument" << endl;
    return 1;
  }
  
  Reader_NRMiner res;
  res.read(argv[1]);

  return 0;
}


#endif
