/*
 *  Main authors:
 *    Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Copyright:
 *      Tias Guns, 2016
 *
 *
 *  This file was part of Fim_cp, Frequent Itemset Mining using
 *  Constraint Programming, and uses Gecode.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <algorithm>
#include <set>
#include <map>
#include <iostream>
#include <fstream>

#include "common/fmrdb_basic.hh"
#include "common/constraint_coverglobal.hpp"
#include "constraint_maxent.hpp"


void Fmrdb_basic::run(const Options_maxent& opt) {
  common_construction(opt);
  const vector<Relation>& rels = reader->rels;
  int num_entities = entities.size();
  
  // entity centric view
  vector< vector<int> > entity_relids(num_entities);
  for (size_t r=0; r!=rels.size(); r++) {
    entity_relids[rels[r].ent1_id].push_back( r );
    entity_relids[rels[r].ent2_id].push_back( r );
  }
  // rel to tdb
  // TODO: avoid this
  vector< vector<BitVec> > rel_tdbs;
  for (size_t r=0; r!=rels.size(); r++)
    rel_tdbs.push_back( rels[r].to_tdb() );
  
  /*********************************************************************/
  // constraints
  // whether a type has an entity
  BoolVarArray type_hasent(*this, num_entities, 0, 1);
  for (int e = 0; e != num_entities; e++)
    rel(*this, BOT_OR, entities[e], type_hasent[e]);
  
  for (int e=0; e!=num_entities; e++) {
      // neigh_hasent and auxiliary coverage for 'all zero' filter:
      // if all neigh (and me) no entities, then ignore the coverage
      BoolVarArgs neigh_hasent;
      neigh_hasent << type_hasent[e];
      BoolVarArray aux_cover(*this, entities[e].size(), 0, 1);
    
      // new: join it all together!
      BoolVarArgs allItems;
      vector<BitVec> allvTdb;
      for (size_t x=0; x!=entity_relids[e].size(); x++) {
        int relid = entity_relids[e][x];
        if (rels[relid].ent1_id == e) {
          // we are rows (first dimension), so append vtdb
          allItems << entities[rels[relid].ent2_id];
          neigh_hasent << type_hasent[rels[relid].ent2_id];
          const vector<BitVec>& vtdb = rels[relid].to_vtdb();
          allvTdb.insert(allvTdb.end(), vtdb.begin(), vtdb.end());
        } else { // if (rels[relid].ent2_id == e) {
          // we are cols (second dimension), so tdb is vtdb for us
          allItems << entities[rels[relid].ent1_id];
          neigh_hasent << type_hasent[rels[relid].ent1_id];
          const vector<BitVec>& vtdb = rels[relid].to_tdb();
          allvTdb.insert(allvTdb.end(), vtdb.begin(), vtdb.end());
        }
      }
      coverage_global(*this, allvTdb, allItems, aux_cover, true);
      
      BoolVar ent_hasneigh(*this, 0, 1);
      rel(*this, BOT_OR, neigh_hasent, ent_hasneigh);
      for (int n=0; n!=entities[e].size(); n++)
        rel(*this, aux_cover[n], BOT_AND, ent_hasneigh, entities[e][n]);
  }
  
  // no empty sets (automatically connected)
  if (opt.noempty() == 1) {
    // TODO: we should ideally add cardinality constraints 'reified' (see old degree_min_constraint())
    for (int e = 0; e != num_entities; e++)
      rel(*this, BOT_OR, entities[e], 1);
  
    
  } else {
    linear(*this, type_hasent, IRT_GQ, 2); // at least two entity types
    
    // enforce connectedness across entity types
    set< vector<int> > paths;
    vector< vector<int> > neighbors(num_entities);
    for (int e = 0; e != num_entities; e++) {
      for (size_t x=0; x!=entity_relids[e].size(); x++) {
        int relid = entity_relids[e][x];
        if (rels[relid].ent1_id == e)
          neighbors[e].push_back(rels[relid].ent2_id);
        else
          neighbors[e].push_back(rels[relid].ent1_id);
      }
    }
    
    for (int e = 0; e != num_entities-1; e++) // -1 because path[0] < path[-1] below
      add_path(e, neighbors, paths);
    
    set< vector<int> >::iterator it;
    for (it = paths.begin(); it != paths.end(); ++it)
    {
      const vector<int>& path = *it;
      size_t path_size = path.size();
      if (path_size > 2 && path[0] < path[path_size-1]) {
        // if the first and last ents of the path have entities
        // so must all entities in between
        // path[0] && path[-1] -> path[1:-1]
        // == path[1:-1] \/ !path[0] \/ !path[-1]
        BoolVarArgs pos;
        if (path_size == 3) {
          pos << type_hasent[path[1]];
        } else {
          BoolVar aux(*this, 0, 1);
          BoolVarArgs midents;
          for (size_t i=0; i!= path_size-1; i++)
            midents << type_hasent[path[i]];
          rel(*this, BOT_AND, midents, aux);
          pos << aux; // aux <-> and(path[1:-1])
          
          // TODO: check if other path with same start/stop,
          // if so, add its aux <-> and(midents) to pos as well!
          set< vector<int> >::iterator otherit;
          for (otherit = paths.begin(); otherit != paths.end(); ++otherit) {
            const vector<int>& otherpath = *otherit;
            if (otherit != it && otherpath.size() == path_size &&
                otherpath[0] == path[0] && otherpath[path_size-1] == path[path_size-1]) {
              cout << "Multiple paths between "<<path[0]<<" and "<<path[path_size-1]<<"...\n";
              BoolVar otheraux(*this, 0, 1);
              BoolVarArgs othermidents;
              for (size_t i=0; i!= path_size-1; i++)
                othermidents << type_hasent[otherpath[i]];
              rel(*this, BOT_AND, othermidents, otheraux);
              pos << otheraux; // otheraux <-> and(otherpath[1:-1])
            }
          }
        }
        BoolVarArgs neg;
        neg << type_hasent[path[0]] << type_hasent[path[path_size-1]];
        clause(*this, BOT_OR, pos, neg, 1);
      }
    }
  }
  

  /**********************************************************************/
  // objective function
  
  // actual objective constraint
  double obj_max = 0;
  for (size_t r=0; r!=rels.size(); r++) {
    for (size_t i=0; i!=rels[r].edge_weights.size(); i++)
      obj_max += rels[r].edge_weights[i];
  }
  
  obj = FloatVar(*this, opt.minent(), obj_max); 
  constraint_maxent(*this, obj, entities, rels, (OptBound)opt.bnd());


  /**********************************************************************/
  // search
  Branchings branching = (Branchings)opt.branching();
  if (branching == B_AUTO) {
    branching = B_JOINT_INVDEGREE_MAX;
    if (opt.bnd() == BND_blacky)
      branching = B_MINSIZE_INVDEG; // based on some tests
  }
  
  if (branching == B_INPUT) {
    // We make an array with the variables of all entities joined
    BoolVarArgs join;
    for (int e = 0; e != num_entities; e++)
      join << entities[e];
    
    branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
  } else {
    // init degrees per entity type
    vector< vector<int> > ent_degrees(entities.size());\
    for (int e = 0; e != num_entities; e++)
      ent_degrees[e].resize(entities[e].size(), 0);
    
    // get degrees
    for (size_t r=0; r!=rels.size(); r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;
      for (size_t i=0; i!=rels[r].edges.size(); i++) {
        ent_degrees[e1][rels[r].edges[i].first]++;
        ent_degrees[e2][rels[r].edges[i].second]++;
      }
    }
    
    if (branching != B_JOINT_DEGREE_MIN) {
      // substract full count of other in rel (so it becomes complement of degree)
      vector<int> maxdeg(num_entities, 0);
      for (size_t r=0; r!=rels.size(); r++) {
        maxdeg[rels[r].ent1_id] += rels[r].ent2_size;
        maxdeg[rels[r].ent2_id] += rels[r].ent1_size;
      }
      for (int e = 0; e != num_entities; e++) {
        int s = maxdeg[e];
        for (int x=0; x!=entities[e].size(); x++)
          ent_degrees[e][x] = ent_degrees[e][x]-s;
      }
    }
    
    // joint branchers
    if (branching == B_JOINT_DEGREE_MIN || branching == B_JOINT_INVDEGREE_MAX) {
      vector< pair<int, pair<int,int> > > indices;
      for (int e = 0; e != num_entities; e++) {
        indices.reserve(indices.size()+entities[e].size());
        for (int n=0; n!=entities[e].size(); n++) {
          pair<int,int> p (e,n);
          indices.push_back( make_pair(ent_degrees[e][n],p));
        }
      }
      std::sort(indices.begin(), indices.end());
      
      BoolVarArgs join;
      for (size_t i=0; i!=indices.size(); i++)
        join << entities[indices[i].second.first][indices[i].second.second];
      branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
    }
    
    // by entity branchers
    if (branching == B_MINSIZE_INVDEG) {
      // first from small to large entity size
      vector< pair<int,int> > ent_indices; ent_indices.reserve(num_entities);
      for (int e = 0; e != num_entities; e++)
        ent_indices.push_back( make_pair(entities[e].size(), e) );
      std::sort(ent_indices.begin(), ent_indices.end());
      
      for (size_t i=0; i!=ent_indices.size(); i++) {
        int e = ent_indices[i].second;
        // then, from large to small ent_degree (either deg or invdeg)
        vector< pair<int,int> > indices;
        indices.reserve(entities[e].size());
        for (int n=0; n!=entities[e].size(); n++)
          indices.push_back( make_pair(-ent_degrees[e][n],n));
        std::sort(indices.begin(), indices.end());
        
        BoolVarArgs join;
        for (size_t i=0; i!=indices.size(); i++)
          join << entities[e][indices[i].second];
        branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
      }
      
    } else if (branching == B_MAXREL_INVDEG) {
      // first from large to small nr relations
      vector<int> ent_rels(num_entities);
      for (size_t r=0; r!=rels.size(); r++) {
        ent_rels[rels[r].ent1_id]++;
        ent_rels[rels[r].ent2_id]++;
      }
      vector< pair<int,int> > ent_indices; ent_indices.reserve(num_entities);
      for (int e = 0; e != num_entities; e++)
        ent_indices.push_back( make_pair(-ent_rels[e], e) );
      std::sort(ent_indices.begin(), ent_indices.end());
      
      for (size_t i=0; i!=ent_indices.size(); i++) {
        int e = ent_indices[i].second;
        // then, from large to small ent_degree (either deg or invdeg)
        vector< pair<int,int> > indices;
        indices.reserve(entities[e].size());
        for (int n=0; n!=entities[e].size(); n++)
          indices.push_back( make_pair(-ent_degrees[e][n],n));
        std::sort(indices.begin(), indices.end());
        
        BoolVarArgs join;
        for (size_t i=0; i!=indices.size(); i++)
          join << entities[e][indices[i].second];
        branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
      }
    }
  }
  
}

int main(int argc, char* argv[]) {
  Options_maxent opt(strchr(argv[0],'/')+1);
  opt.datafile("../data/main.txt");
  opt.description("This model finds relational frequent patterns, given 3 entities and 2 relations, with no additional constraints");
  opt.usage("-datafile somedir");
  opt.step(0); // precision in BAB
  opt.parse(argc, argv);
  // WARNING, the manual work below ignores certain input parameters now, e.g. solfile, statfile, gist, ...
  
  // read in the data and obtain the data structure:
  Reader_NRMiner* reader = new Reader_NRMiner(); 
  reader->read(opt.datafile());

  // open output file
  streambuf* buf;
  ofstream of;
  if (strcmp(opt.solfile(), "") != 0) {
    of.open(opt.solfile());
    buf = of.rdbuf();
  } else {
    buf = cout.rdbuf();
  }
  ostream l_out(buf);

  // We can probably also compute the _values here in CPP directly instead of reading form the files...
  float init_minent = opt.minent();
  
  // compute DL_mult, DL_add
  double DL_mult = 0;
  double DL_add = 0;
  {
    double nbOfEdges = 0;
    double nbOfPossibleEdges = 0;
    vector<double> ent_sizes(reader->ent_names.size(), 0);
    for (size_t r=0; r!=reader->rels.size(); r++) {
      const Relation& rel = reader->rels[r];
      nbOfEdges += rel.edges.size();
      ent_sizes[rel.ent1_id] = rel.ent1_size;
      ent_sizes[rel.ent2_id] = rel.ent2_size;
      nbOfPossibleEdges += ( (double)rel.ent1_size * (double)rel.ent2_size ); // can overflow int
    }
    double density = nbOfEdges/nbOfPossibleEdges;
    
    DL_mult = log((1-density)/density);
    for (size_t e=0; e!=ent_sizes.size(); e++)
      DL_add += ent_sizes[e];
    DL_add *= log(1/(1-density));
  }  
  // we will store all found patterns across all iterations
  vector< vector< vector<int> > > patterns; // patterns[i] = vec[type][e1,e2,e3,...]
  vector< Fmrdb_basic* > best_sols;
  
  // for BAB<> (must be _Search::_Options)
  Search::Options sopt;
  sopt.threads = opt.threads();
  sopt.c_d = opt.c_d();
  sopt.a_d = opt.a_d();
  sopt.stop    = CombinedStop::create(opt.node(),opt.fail(), opt.time(),
                                      opt.interrupt());
  
  Fmrdb_basic* model;
  unsigned int this_iter = 0;
  //model = new Fmrdb_basic(opt);
  while (true) {
    this_iter++;
    Support::Timer t;
    t.start();
    
    // Here we can pass anything we want to the model, e.g. Reader_NRMiner*
    model = new Fmrdb_basic(opt, reader);
    
    // the BaB solving (continue till proven optimal)
    BAB<Fmrdb_basic> engine(model,sopt);
    int nr_sols = 0;
    
    Fmrdb_basic* best_sol = NULL;
    while (Fmrdb_basic* sol = engine.next()) {
      nr_sols++;
      sol->print(cout);
      // Here you can do anything you want, e.g. store the solution in a vector
      patterns.push_back( sol->get_pattern() );
      delete best_sol;
      best_sol = sol;
    }
    l_out << "Iteration " << this_iter << " best solution is :" << endl;
    best_sol->print(l_out);
    best_sols.push_back(best_sol);

    Search::Statistics stats = engine.statistics();
    // output stats, copy paste of script driver below
    l_out << endl << "Initial" << endl
    << "\tpropagators: " << model->propagators() << endl;
    l_out << "\tbranchers:   " << model->branchers() << endl;
    l_out << endl
    << "Summary" << endl
    << "\truntime:      ";
    stop(t, l_out);
    l_out << endl
    << "\tsolutions:    " << nr_sols << endl
    << "\tpropagations: " << stats.propagate << endl
    << "\tnodes:        " << stats.node << endl
    << "\tfailures:     " << stats.fail << endl
    << "\trestarts:     " << stats.restart << endl
    << "\tno-goods:     " << stats.nogood << endl
    << "\tpeak depth:   " << stats.depth << endl
    << endl;
    delete model;
    
    if (this_iter == opt.iter())
      break; // done
      
    // Here we can update the probabilities in reader->rels
    for (size_t r = 0; r != best_sol->reader->rels.size(); r++) {
      int e1 = best_sol->reader->rels[r].ent1_id;
      int e2 = best_sol->reader->rels[r].ent2_id;

      for (size_t i = 0; i != best_sol->reader->rels[r].edges.size(); i++) {
        int val1 = -1; int val2 = -1; // both unassigned
        if (best_sol->entities[e1][ reader->rels[r].edges[i].first ].assigned())
          val1 = best_sol->entities[e1][ reader->rels[r].edges[i].first ].val();
        if (best_sol->entities[e2][ reader->rels[r].edges[i].second ].assigned())
          val2 = best_sol->entities[e2][ reader->rels[r].edges[i].second ].val();
	if (val1 == 1 && val2 == 1){
	  reader->rels[r].edge_weights[i] = 0;
	}
      }
      reader->rels[r].sort_yourself();
    }

    //Compute interestingness of each of the patterns
    // TODO!! this only makes sense after setting edges to 0...
    float minent = init_minent;
    const vector<Relation>& rels = reader->rels;
    for (size_t p=0; p!=patterns.size(); p++) {
      const vector< vector<int> >& pattern = patterns[p];
      double pattern_int = 0;
      int nb_ent = 0;
      for (size_t e=0;e!=pattern.size();e++) {
        for (size_t i=0;i!=pattern[e].size();i++)
          nb_ent++;
      }
      for (size_t r = 0; r != rels.size(); r++) {
        for (size_t n_edge = 0; n_edge != rels[r].edges.size(); n_edge++) {
          if (
            (find(pattern[rels[r].ent1_id].begin(), pattern[rels[r].ent1_id].end(), rels[r].edges[n_edge].first)
            != pattern[rels[r].ent1_id].end()) &&
            (find(pattern[rels[r].ent2_id].begin(), pattern[rels[r].ent2_id].end(), rels[r].edges[n_edge].second)
            != pattern[rels[r].ent2_id].end())
          ) {
            pattern_int += rels[r].edge_weights[n_edge];
          }
        }
      }
      minent = max(minent, float(floor(1000000*pattern_int/(DL_add+DL_mult*nb_ent))/1000000));
    }
    
    cout << "New min ent: " << minent << " (old: "<<opt.minent()<<")\n";
    opt.minent(minent);
  }
 
  /*cout << "Summary of the solutions :" << endl;
  for (size_t i=0; i != best_sols.size(); i++) {
    best_sols[i]->print(cout);
    delete best_sols[i];
  }*/


  delete sopt.stop;
  
  return 0;
}
