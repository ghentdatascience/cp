//
// Global for MaxEnt computation
// 
// Made by Tias Guns <tias.guns@cs.kuleuven.be>

#ifndef _CONS_MAXENT_
#define _CONS_MAXENT_


#include <gecode/int.hh>
#include <gecode/float.hh>

#include "common/fmrdb_basic.hh"

using namespace Gecode; 
using namespace Int;
using namespace std; 

// post, constraint over relations
void constraint_maxent(Space& home, 
                       FloatVar& obj_, 
                       vector< BoolVarArray >& entities_,
                       const vector<Relation>& rels,
                       const OptBound opt_bnd
                      );


template <class VA>
class ConsMaxEnt : public Gecode::Propagator {
protected:
  Float::FloatView obj;
  vector< ViewArray<VA> > entities;
  const vector<Relation> rels; // constant, make pointer?
  vector<BitVec> rels_free;
  double base_val;
  const double DL_mult;
  const double DL_add;
  const OptBound opt_bnd;  

public:
    // posting
    static Gecode::ExecStatus post(Gecode::Space& home,
                                   Float::FloatView& obj,
                                   vector< ViewArray<VA> >& entities,
                                   const vector<Relation>& rels,
                                   const double DL_mult,
                                   const double DL_add,
                                   const OptBound opt_bnd
                                  ) {
      (void) new (home) ConsMaxEnt<VA>(home,obj,entities,rels,DL_mult,DL_add,opt_bnd);
      return Gecode::ES_OK;
    }
    
    // post constructor
    ConsMaxEnt(Gecode::Space& home,
               Float::FloatView& obj0,
               vector< ViewArray<VA> >& entities0,
               const vector<Relation>& rels0,
               const double DL_mult0,
               const double DL_add0,
               const OptBound opt_bnd0
               )
    : Propagator(home), obj(obj0), entities(entities0), rels(rels0), rels_free(rels.size()), base_val(0), DL_mult(DL_mult0), DL_add(DL_add0), opt_bnd(opt_bnd0)
    {
      for (size_t r=0; r!=rels.size(); r++)
        rels_free[r].resize(rels[r].edges.size(), true);
      
      home.notice(*this,Gecode::AP_DISPOSE); // so that the vectors are properly disposed
      obj.subscribe(home,*this,Gecode::Float::PC_FLOAT_BND);
      for (size_t e=0; e!=entities.size(); e++)
        entities[e].subscribe(home,*this,Gecode::Int::PC_INT_VAL);
    }
    
    // copy constructor
    ConsMaxEnt(Gecode::Space& home, bool share, ConsMaxEnt& p)
    : Propagator(home,share,p), entities(p.entities), rels(p.rels), rels_free(p.rels_free), base_val(p.base_val), DL_mult(p.DL_mult), DL_add(p.DL_add), opt_bnd(p.opt_bnd)
    {
      obj.update(home,share,p.obj);
      for (size_t e=0; e!=p.entities.size(); e++)
        entities[e].update(home,share,p.entities[e]);
    }
    
    virtual size_t dispose(Gecode::Space& home)
    {
      home.ignore(*this,Gecode::AP_DISPOSE);
      obj.cancel(home,*this,Gecode::Float::PC_FLOAT_BND);
      for (size_t e=0; e!=entities.size(); e++)
        entities[e].cancel(home,*this,Gecode::Int::PC_INT_VAL);
      entities.~vector();
      rels_free.~vector();
      rels.~vector(); // TODO,needed?
      (void) Propagator::dispose(home);
      return sizeof(*this);
    }

    virtual Gecode::Propagator* copy(Gecode::Space& home, bool share)
    {
      return new (home) ConsMaxEnt<VA>(home,share,*this);
    }

    virtual Gecode::PropCost cost(const Gecode::Space&, const Gecode::ModEventDelta&) const
    {
      // run as last
      return Gecode::PropCost::cubic(Gecode::PropCost::HI, 10000);
    }
    
  
    // propagation
    virtual Gecode::ExecStatus propagate(Gecode::Space& home, const Gecode::ModEventDelta&);    
    
    double prop_naieve_orig(const vector<int>& size_assigned);
    double prop_naieve(const vector<int>& size_assigned, const vector<int>& size_compatible);
    double prop_linear(const vector<int>& size_assigned, const vector<int>& size_compatible);
    double prop_full(const vector<int>& size_assigned, const vector<int>& size_compatible);
    double prop_black(const vector<int>& size_assigned, const vector<int>& size_compatible);
    double prop_blacky(const vector<int>& size_assigned, const vector<int>& size_compatible, Space& home);
    double prop_look(const vector<int>& size_assigned, const vector<int>& size_compatible, Space& home, double curval);
    
    bool is_a_valid_combination(const vector<int>* n_add, 
	    const vector<pair<vector<int>, vector<int> > >* degree1, size_t e);
    void edges_degrees(int r, const Relation* rel, vector<int>& degree1, vector<int>& degree2);
    int get_max_edges_if1(int size_assigned1, const vector<int>& degree1,
                          int size_assigned2, const vector<int>& degree2,
                          int cap=-1 // OPTIONAL!
                         );
};

#endif
