//
// constraint_coverglobal.hpp
//
// global 'cover' constraint (also cover_inv for closed)
// 
// Made by Tias Guns <tias.guns@cs.kuleuven.be>

#ifndef _COVER_GLOBAL_HPP_
#define _COVER_GLOBAL_HPP_

#include <gecode/int.hh>

#include <boost/dynamic_bitset.hpp>
typedef boost::dynamic_bitset<> BitVec;

using namespace std;

// post, constraint over entire matrix
void coverage_global(Gecode::Space& home, 
                     const vector<BitVec>& tdb, 
                     const Gecode::BoolVarArgs& items0, 
                     const Gecode::BoolVarArgs& trans0,
                     bool transpose = false
                    );

// propagator for connecting single subproblem
template <class VA, class VB>
class CoverGlobal : public Gecode::Propagator {
protected:
  BitVec** vdata;
  Gecode::ViewArray<VA> items;
  Gecode::ViewArray<VB> trans;
  int nr_t;
  BitVec items_free;
  BitVec trans_free;

public:
    // posting
    static Gecode::ExecStatus post(Gecode::Space& home,
                                   BitVec** vdata,
                                   Gecode::ViewArray<VA> items,
                                   Gecode::ViewArray<VB> trans
                                  ) {
      (void) new (home) CoverGlobal<VA,VB>(home,vdata,items,trans);
      return Gecode::ES_OK;
    }
    
    // post constructor
    CoverGlobal(Gecode::Space& home,
                BitVec** vdata0,
                Gecode::ViewArray<VA> items0,
                Gecode::ViewArray<VB> trans0
               )
    : Propagator(home), vdata(vdata0), items(items0), trans(trans0), nr_t(trans.size()), items_free(), trans_free()
    {
      home.notice(*this,Gecode::AP_DISPOSE); // so that the vectors are properly disposed
      items.subscribe(home,*this,Gecode::Int::PC_INT_VAL);
      trans.subscribe(home,*this,Gecode::Int::PC_INT_VAL);
      items_free.resize(items.size(), -1); // all 1
      trans_free.resize(trans.size(), -1); // all 1
    }
    
    // copy constructor
    CoverGlobal(Gecode::Space& home, bool share, CoverGlobal& p)
    : Propagator(home,share,p), vdata(p.vdata), nr_t(p.nr_t), items_free(p.items_free), trans_free(p.trans_free)
    {
      items.update(home,share,p.items);
      trans.update(home,share,p.trans);
      /*
      // in constructor, we allocated nr_t places for trans
      // costly to update all, so only update the ones we will use
      size_t t = trans_free.find_first();
      while (t != BitVec::npos) {
        trans[t].update(home, share, p.trans[t]);
        t = trans_free.find_next(t);
      }
      */
    }
    
    virtual size_t dispose(Gecode::Space& home)
    {
      home.ignore(*this,Gecode::AP_DISPOSE);
      items.cancel(home,*this,Gecode::Int::PC_INT_VAL);
      trans.cancel(home,*this,Gecode::Int::PC_INT_VAL);
      items_free.~dynamic_bitset();
      trans_free.~dynamic_bitset();
      (void) Propagator::dispose(home);
      return sizeof(*this);
    }

    virtual Gecode::Propagator* copy(Gecode::Space& home, bool share)
    {
      return new (home) CoverGlobal<VA,VB>(home,share,*this);
    }

    virtual Gecode::PropCost cost(const Gecode::Space&, const Gecode::ModEventDelta&) const
    {
      // run as last
      return Gecode::PropCost::quadratic(Gecode::PropCost::HI, items.size()*trans.size());
    }
    
  
    // propagation
    virtual Gecode::ExecStatus propagate(Gecode::Space& home, const Gecode::ModEventDelta&);    
    
};

#endif
