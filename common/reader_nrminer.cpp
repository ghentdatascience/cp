/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Some code based N-RMiner's CSVReader.cpp (by Eirini Spyropoulou)
 * 
 *  Copyright:
 *      all authors
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "common/reader_nrminer.hh"

using namespace std;


// http://stackoverflow.com/questions/874134/find-if-string-ends-with-another-string-in-c
inline bool ends_with(std::string const & value, std::string const & ending)
{
  if (ending.size() > value.size()) return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}


void Reader_NRMiner::read (const char* dirname) {

  const vector<string> relfiles = list_dir(dirname);
  //for (size_t i=0; i!=relfiles.size(); i++)
  //  cout << "have relfile ["<<i<<"]: "<<relfiles[i]<<"\n";
  
  for (size_t i=0; i!=relfiles.size(); i++)
    read_relfile(relfiles[i]);
  
  // set reverse ent_ids: ent_names
  ent_names.resize(ent_ids.size());
  for (map<string, int>::iterator it=ent_ids.begin(); it!=ent_ids.end(); it++) {
    ent_names[it->second] = it->first.c_str();
  }

  // set reverse correspondence: corresponding_names
  corresponding_names.resize(ent_ids.size());
  for (int e=0; e!=(int)ent_ids.size(); e++) {
    corresponding_names[e].resize(correspondence[e].size());
    for (map<string, int>::iterator it=correspondence[e].begin(); it!=correspondence[e].end(); it++) {
      corresponding_names[e][it->second-1] = it->first.c_str(); // offset 1 vs offset 0
    }
  }
  
  // sort each rel by weight (decreasing)
  for (size_t r=0; r!=rels.size(); r++)
    rels[r].sort_yourself();
  
  // sync all rel.entX_size to consistent values
  int nr_ent = ent_ids.size();
  vector<int> sizes(nr_ent, 0);
  for (size_t r=0; r!=rels.size(); r++) { // collect
    if (rels[r].ent1_size > sizes[rels[r].ent1_id])
      sizes[rels[r].ent1_id] = rels[r].ent1_size;
    if (rels[r].ent2_size > sizes[rels[r].ent2_id])
      sizes[rels[r].ent2_id] = rels[r].ent2_size;
  }
  for (size_t r=0; r!=rels.size(); r++) { // collect
    rels[r].ent1_size = sizes[rels[r].ent1_id];
    rels[r].ent2_size = sizes[rels[r].ent2_id];
  }
  
  //print();
  /*
  cout << "new index -> old index" << endl;
  for (int i = 0; i < 100; i++) {
    for (map<int,int>::iterator it = correspondence[0].begin(); it != correspondence[0].end(); it++) {
      if (it->second == i)
	cout << i << "->" << it->first << endl;
    }
  }*/
}

vector<string> Reader_NRMiner::list_dir(const char* dirname)
{
  printf("Reading relations from '%s'\n", dirname);
  DIR* dp;
  dirent* dirp;
  vector<string> relfiles;
  
  if((dp  = opendir(dirname)) == NULL) {
    cout << "Error(" << errno << ") nary opening directory: " <<dirname << endl;
    return relfiles;
  }
  
  while ((dirp = readdir(dp)) != NULL) {
    if (ends_with(dirp->d_name, ".txt") || ends_with(dirp->d_name, ".csv")) {
      string filename = string(dirname)+"/"+string(dirp->d_name);
      relfiles.push_back(filename);
    }
  }
  closedir(dp);
  
  return relfiles;
}

void Reader_NRMiner::read_relfile(const string& relfile)
{
  cout << "Reading: "<<relfile; // size and newline at end of function
  // read header
  string header;
  fstream narystream(relfile.c_str(),ios::in);
  getline(narystream, header);
  if (header.find('\r')!=string::npos || header.find('\n')!=string::npos)
    header.erase(header.length()-1);
  //cout << "Got header:"<<header<<"\n";
  
  Relation* rel = NULL; // placeholder (existing or new)
  
  // check if already a matching rel (e.g. values or edges already read)
  for (size_t r=0; r!=rels.size(); r++) {
    if (rels[r].header == header) {
      rel = &rels[r];
      break;
    }
  }
  
  // if rel does not exist yet, make it
  if (rel == NULL) {
    //cout << "no Rel with header '"<<header<<"' yet\n";
    
    // read two entity names from header, turn into ids
    stringstream line_stream(header);
    string entname;
    int entid[2] = {0,0};
    int i=0;
    while (getline(line_stream, entname, ',')) {
      if (i > 1) cout << "Error, too many entities while reading file\n";
      if (ent_ids.count(entname) == 0) {
        int id=(int)ent_ids.size();
        ent_ids[entname] = id;
	map<string,int> corr_id;
	correspondence.push_back(corr_id);
      }
      entid[i++] = ent_ids[entname];
    }
    
    // make Rel
    rels.push_back( Relation(header, entid[0], entid[1]) );
    rel = &rels[rels.size()-1];
  }
  
  // read in rest
  string line;
  getline(narystream, line);
  // check if edges or weights
  size_t comma_pos = line.find(',');
  if (comma_pos == string::npos) {
    // read weights
    //cout << "Weights\n";
    do {
      double w = atof(line.c_str());
      rel->edge_weights.push_back( w );
    } while (getline(narystream, line));
  } else {
    // read edges
    //cout << "Edges\n";
    string node_str;
    int max1 = 0;
    int max2 = 0;
    do {
      if (line.find("\r") != line.npos || line.find("\n") != line.npos)
	line.erase(line.length()-1);

      stringstream line_stream(line);
      int node1;
      int node2;
      // New version : map from string to int
      string node1_file;
      string node2_file;
      getline(line_stream, node1_file, ',');
      getline(line_stream, node2_file, ',');
      // Old version : map from int to int (see lines 67 and 177 also)
      /*int node1_file;
      int node2_file;
      getline(line_stream, node_str, ',');
      node1_file = atoi(node_str.c_str());
      getline(line_stream, node_str, ',');
      node2_file = atoi(node_str.c_str());*/

      //Compute the corresponding id to make the numerotation continuous
      if (correspondence[rel->ent1_id].count(node1_file) == 0) {
        int id=(int)correspondence[rel->ent1_id].size();
        correspondence[rel->ent1_id][node1_file] = id+1;
      }
      node1 = correspondence[rel->ent1_id][node1_file];

      if (correspondence[rel->ent2_id].count(node2_file) == 0) {
        int id=(int)correspondence[rel->ent2_id].size();
        correspondence[rel->ent2_id][node2_file] = id+1;
      }
      node2 = correspondence[rel->ent2_id][node2_file];

      if (node1 == 0 || node2 == 0)
        cout << "ERROR: data assumed to be offset 1, but found node with id '0'!!\n";
      if (max1 < node1)
        max1 = node1;
      if (max2 < node2)
        max2 = node2;
      rel->edges.push_back( make_pair<int,int>(node1-1,node2-1) ); // data is offset 1!
    } while (getline(narystream, line));
    // size = max (because max of offset 1, while actual data is not)
    rel->ent1_size = max1;
    rel->ent2_size = max2;
  }
  if (rel->ent1_size != 0)
    cout << " :: "<<rel->ent1_size<<" x "<<rel->ent2_size<<" (density: "<<double(rel->edges.size())/rel->ent1_size/rel->ent2_size<<")";
  cout << "\n"; // close cout at beginning
}

void Reader_NRMiner::print()
{
  for (map<string, int>::iterator iter = ent_ids.begin(); iter != ent_ids.end(); ++iter )
    cout << "Entity: "<<iter->first<<" has id: "<<iter->second<<"\n";
  
  for (size_t r=0; r!=rels.size(); r++)
    rels[r].print();
}

