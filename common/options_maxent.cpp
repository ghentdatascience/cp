/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Copyright:
 *      Tias Guns, 2008
 *
 *  Revision information:
 *      $Id: options_fimcp.cpp 305 2009-12-16 15:16:10Z tias $
 *
 *  This file is part of Fim_cp, Frequent Itemset Mining using
 *  Constraint Programming, and uses Gecode.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __FIMCPLIKE_COMMON_OPTIONS__
#define __FIMCPLIKE_COMMON_OPTIONS__

#include <cmath>
#include <gecode/driver.hh>
using namespace Gecode;

enum OutputType {
    OUT_NONE,
    OUT_FIMI,
    OUT_FULL,
    OUT_CPVARS,
};

enum Branchings {
  B_AUTO,
  B_INPUT,
  B_JOINT_DEGREE_MIN,
  B_JOINT_INVDEGREE_MAX,
  B_MINSIZE_INVDEG,
  B_MAXREL_INVDEG
};


/**
 * \brief Options for examples with additional size parameter
 *
 */
class Options_maxent : public Options {
protected:
    StringOption _output;

    vector<BaseOption*> _list_specific;
    const char* _description;
    const char* _usage;

    // FIM_CP options
    StringValueOption _datafile;
    StringValueOption _solfile;
    UnsignedIntOption _cclause;
    UnsignedIntOption _noempty;
    DoubleOption _minent;
    StringOption _bnd;
    UnsignedIntOption _iter;

public:
  /// Initialize options for example with name \a e
  Options_maxent(const char* e)
  : Options(e)
  , _output("-output", "type of output of solutions", OUT_FIMI)
  , _list_specific() // branching
  // defaults (must be add_specific() to be used)
  , _datafile("-datafile", "directory with datasets to use", "example/")
  , _solfile("-solfile", "filename to write solutions to (any name)", "")
  , _cclause("-cclause", "coverage constraint using clause ? (not used)", 1)
  , _noempty("-noempty", "no empty entities (0=allow some empty, 1=no empty ents)", 0)
  , _minent("-minent", "minimum maximum entropy score", 0.0)
  , _bnd("-bnd", "bound to use", BND_blacky)
  , _iter("-iter", "number of iterations of finding the best pattern and updating the distribution, default=1 (=0 infinitely)", 1) {

  // set defaults
  solutions(0);
  iterations(10); // used with -mode time
  c_d(0); // with branchval MAX: less memory (sometimes faster)

  // general options
  _branching.add(B_AUTO, "auto");
  _branching.add(B_INPUT, "input");
  _branching.add(B_JOINT_INVDEGREE_MAX, "invdeg");
  _branching.add(B_JOINT_DEGREE_MIN, "deg");
  _branching.add(B_MINSIZE_INVDEG, "bysize");
  _branching.add(B_MAXREL_INVDEG, "byrel");
  branching(B_AUTO);

  _output.add(OUT_NONE, "none", "do not output solutions");
  _output.add(OUT_FIMI, "normal", "print solutions (FIMI-style)");
  _output.add(OUT_FULL, "full", "print solutions (FIMI-style) and transaction sets");
  _output.add(OUT_CPVARS, "cpvars", "print the CP variables of the solutions)");
  add_specific(_output);

  add_specific(_datafile);
  add_specific(_solfile);
  add_specific(_cclause);
  
  add_specific(_noempty);
  add_specific(_minent);
  
  _bnd.add(BND_naieve, "naieve", "naieve bound (but with maxarea)");
  _bnd.add(BND_naieve_orig, "naieve2", "really naieve bound (all remaining edges");
  _bnd.add(BND_linear, "linear", "linear iteration over nr of entities");
  _bnd.add(BND_full, "full", "full iteration over all number of entities");
  _bnd.add(BND_black, "black", "black/grey edges with full iteration over all number of entities");
  _bnd.add(BND_blacky, "blacky", "black/grey edges with full iteration over all number of entities and semi-look-ahead");
  _bnd.add(BND_look, "look", "look-ahead over all entities");
  add_specific(_bnd);
  add_specific(_iter);
}

  // add the option to our accessible vector
  // for help that uses friend in BaseOption, grrr
  inline void add_specific(BaseOption& o) {
    _list_specific.push_back(&o);
  }
  
  // output
  inline void output(int v) {
    _output.value(v);
    //add_specific() this one is added in constructor
  }
  inline int output(void) const {
    return _output.value();
  }

  // FIM_CP options (added after mark)
  // datafile
  inline void datafile(const char* s) {
    _datafile.value(s);
    //add_specific() this one is added in constructor
  }
  inline const char* datafile(void) const {
    return _datafile.value();
  }

  // solfile
  inline void solfile(const char* s) {
    _solfile.value(s);
    //add_specific() this one is added in constructor
  }
  inline const char* solfile(void) const {
    return _solfile.value();
  }

  // cclause
  inline void cclause(unsigned int v) {
    _cclause.value(v);
    //add_specific() this one is added in constructor
  }
  inline unsigned int cclause(void) const {
    return _cclause.value();
  }

  // noempty
  inline void noempty(unsigned int v) {
    _noempty.value(v);
    add_specific(_noempty);
  }
  inline unsigned int noempty(void) const {
    return _noempty.value();
  }

  // minent
  inline void minent(float v) {
    _minent.value(v);
    add_specific(_minent);
  }
  inline float minent(void) const {
    return _minent.value();
  }
  
  // bnd
  inline void bnd(int v) {
    _bnd.value(v);
    //add_specific() this one is added in constructor
  }
  inline int bnd(void) const {
    return _bnd.value();
  }
  
  inline void iter(unsigned int v) {
    _iter.value(v);
  }
  inline unsigned int iter(void) const {
    return _iter.value();
  }
  
  // description
  inline void description(const char* s) {
    _description = s;
  }

  // usage
  inline void usage(const char* s) {
    _usage = s;
  }


  /// Parse options from arguments \a argv (number is \a argc)
  void parse(int& argc, char* argv[]) {
    int c = argc;
    char** v = argv;
    
    int x = _list_specific.size();

  // horrible code needs horrible goto (copy from original code)
  next:
    for (int i = 0; i != x; i++) {
      if (int a = _list_specific[i]->parse(c,v)) {
        c -= a; v += a;
        goto next;
      }
    }
    // the non-list-specific ones
    Options::parse(c,v);
  }

  virtual void help(void) {
    Options::help();

    std::cerr << "Specific options for " << name() << ":" << std::endl;
    int x = _list_specific.size();
    for (int i = 0; i != x; i++)
      _list_specific[i]->help();
    
    std::cerr << "Description:" << std::endl
              << "    " << _description << std::endl;
    std::cerr << "Usage example:" << std::endl
              << "    " << "./" << name() << " " << _usage << std::endl;
  }
};

#endif
