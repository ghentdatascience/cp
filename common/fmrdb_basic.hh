/*
 *  Main authors:
 *      Aida Jimenez <aidajm@decsai.ugr.es>
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Copyright:
 *      Aida Jimenez 2009
 *
 *  Revision information:
 *      $Id: fmrdb_basic.hh 305 2009-12-16 15:16:10Z tias $
 *
 *  This file is part of Fim_cp, Frequent Itemset Mining using
 *  Constraint Programming, and uses Gecode.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __FMRDB_COMMON_BASIC_HH__
#define __FMRDB_COMMON_BASIC_HH__

#include <cstdio>
#include <iostream>
#include <vector>
#include <set>
using namespace std;

#include <boost/dynamic_bitset.hpp>
typedef boost::dynamic_bitset<> BitVec;

#include <gecode/driver.hh>
using namespace Gecode::Driver;

#include "common/relation.hh"
#include "common/reader_nrminer.hh"

enum OptBound {
  BND_naieve,
  BND_naieve_orig,
  BND_linear,
  BND_full,
  BND_black,
  BND_blacky,
  BND_look
};

#include "options_maxent.cpp"

enum PrintStyle {
    PRINT_NONE,
    PRINT_FIMI,
    PRINT_FULL,
    PRINT_CPVARS
};

BoolVarArgs sort_by_freq(const BoolVarArray& items, const vector< vector<bool> >& tdb, int nr_i);

void add_path(int node, const vector< vector<int> >& neigh, set< vector<int> >& paths, vector<int> prefix=vector<int>());

/**
 * %FIM-CP: Frequent Itemset Mining in CP, base class.
 *
 * Standard frequent itemset mining with off-the-shelve
 * Constraint Programming techniques.
 *
 * All models either:
 *  - only implement Fmrdb_basic::run() {not listed in doxygen}
 *  - inherit Fmrdb_basic and implement run()
 *
 */
class Fmrdb_basic : public Script {
protected:
  /// print itemsets?
  PrintStyle print_itemsets;
  /// file to print to, if any
  FILE* solfile;
  //Reader_NRMiner* reader; // for output printing

  /// entities Variables
  //vector<BoolVarArray> entities;
  bool do_opt; // do optimization
  FloatVar obj;

public:
  Reader_NRMiner* reader; // for output printing

  /// entities Variables
  vector<BoolVarArray> entities;
  /// Constructor for creation
  Fmrdb_basic(const Options_maxent&, Reader_NRMiner* reader=NULL);

  /// Constructor for cloning \a s
  Fmrdb_basic(bool, Fmrdb_basic&);

  /// Perform copying during cloning
  virtual Space* copy(bool share) {
    return new Fmrdb_basic(share,*this);
  }

  /// Do common construction stuff
  //const vector< vector< vector<bool> > > common_construction(const Options_fmrdb&);
  void common_construction(const Options_maxent& opt);
  
  void constrain(const Space& _best) {
    if (do_opt) {
      const Fmrdb_basic* best =
      dynamic_cast<const Fmrdb_basic*>(&_best);
      if (best == NULL)
        throw DynamicCastFailed("Fmrdb_basic::constrain");
      rel(*this, obj, FRT_GR, best->obj.max());
    }
  }
  
  /// Get frequency to use
  int getFreq(const Options_maxent&);

  /// Calculate sparseness of matrix
  float getSparseness(const vector< vector<bool> >&);

  /// Post coverage using the CLAUSE constraint
  void coverage_clause(const vector< vector<bool> >& tdb);

  virtual void run(const Options_maxent&);

  /// Print a solution to \a os
  virtual void print(std::ostream&) const;
  
  /// Return a solution
  virtual vector< vector<int> > get_pattern(); // vec[type] = [e1,e2,e3,...]

  //void print_items(BoolVarArray items, BoolVarArray transactions,vector<bool> classes, int nr_i, int nr_t) const;
  /*****************Constraints functions**************/
  void min_size_constraint(const BoolVarArgs& var, int size);

  ///funtion for the  degree  A-<R>-mindegree-B   input=B, output=A, normal is true if tbd=AxB and false if tbd=BxA
  void degree_min_constraint(const BoolVarArgs& input, const vector<vector<bool> >& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal, int mindeg);

  ///funtion for the maximal degree  A-<R>-[mindegree]-B   input=B, output=A, normal is true if tbd=AxB and false if tbd=BxA
  void maximal_degree_min_constraint(const BoolVarArgs& input, const vector<vector<bool> >& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal, int mindeg);

  ///funtion for the coverage  I=<R>-O   input=I, output=O, tbd=IxO set swap_tdb=true when tdb=OxI
  void coverage_constraint(const BoolVarArgs& input, const vector<BitVec>& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal=true);
  void coverage_constraint(const Relation& rel, const vector<BitVec>& tdb, int e, const BoolVarArgs& target);
  
  vector<int> get_entity_sizes(const vector<Relation>& rels, int nr_ents);
};

#endif
