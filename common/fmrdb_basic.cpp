/*
 *  Main authors:
 *      Aída Jiménez <aidajm@decsai.ugr.es>
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Copyright:
 *      Tias Guns, 2008
 *
 *  Revision information:
 *      $Id: fmrdb_basic.cpp 305 2009-12-16 15:16:10Z tias $
 *
 *  This file is part of Fim_cp, Frequent Itemset Mining using
 *  Constraint Programming, and uses Gecode.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __FMRDB_COMMON_BASIC__
#define __FMRDB_COMMON_BASIC__
#define _FILE_OFFSET_BITS  64

#include "fmrdb_basic.hh"
#include "constraint_coverglobal.hpp"




/// Constructor for creation
Fmrdb_basic::Fmrdb_basic(const Options_maxent& opt, Reader_NRMiner* reader0) :
        Script(opt),
        print_itemsets(PRINT_NONE), solfile(stdout), do_opt(true), reader(reader0) {
    // all stuff implemented in ::run
    run(opt);
}

/// Model must be implemented in this function.
//void fmrdb_basic::run(const Options_maxent& opt) {
//}

/// Constructor for cloning \a s
Fmrdb_basic::Fmrdb_basic(bool share, Fmrdb_basic& s) :
        Script(share,s),
        print_itemsets(s.print_itemsets),
        solfile(s.solfile),
        do_opt(s.do_opt),
        obj(s.obj),
        reader(s.reader),
        entities(s.entities)
{
    for (size_t i=0;i<entities.size();i++)
        entities[i].update(*this, share, s.entities[i]);
    obj.update(*this, share, s.obj);
}

/// Do common construction stuff
void Fmrdb_basic::common_construction(const Options_maxent& opt) {
  // output stuff
  if (opt.output() == OUT_FIMI)
    print_itemsets = PRINT_FIMI;
  else if (opt.output() == OUT_FULL)
    print_itemsets = PRINT_FULL;
  else if (opt.output() == OUT_CPVARS)
    print_itemsets = PRINT_CPVARS;
  #ifdef GECODE_HAS_GIST
  if (opt.mode() == SM_GIST)
    print_itemsets = PRINT_CPVARS;
  #endif
  /*if (strcmp(opt.solfile(),"") != 0) { // solfile != ""
    fprintf(stdout, "writing solutions to file %s\n", opt.solfile());
    solfile = fopen(opt.solfile(),"w");
    if (solfile == NULL)
      throw Exception("Solution file", "Can not open solution file");
  }*/
  
  if (reader == NULL) {
    // Read data (XXX never deleted because passed to all nodes)
    reader = new Reader_NRMiner(); 
    reader->read(opt.datafile());
  }
  
  int num_entities=reader->ent_ids.size();
  const vector<int> size_e = get_entity_sizes(reader->rels, num_entities);
  
  // init entity vars
  for (int i=0;i<num_entities;i++)
    entities.push_back( BoolVarArray(*this, size_e[i], 0, 1) );
}

vector<int> Fmrdb_basic::get_entity_sizes(const vector<Relation>& rels, int nr_ents)
{
  vector<int> sizes(nr_ents, 0);
  for (size_t r=0; r!=rels.size(); r++) {
    sizes[rels[r].ent1_id] = rels[r].ent1_size;
    sizes[rels[r].ent2_id] = rels[r].ent2_size;
  }
  return sizes;
}


/*TODO
/// Calculate sparseness of matrix
inline
float fmrdb_basic::getSparseness(const vector< vector<bool> >& tdb) {
    int sum = 0;
    for (int t=0; t!=nr_t; t++) {
        for (int i=0; i!=nr_i; i++)
            sum += tdb[t][i];
    }
    return ((float)sum/(nr_i*nr_t));
}

/// Post coverage using the CLAUSE constraint
void fmrdb_basic::coverage_clause(const vector< vector<bool> >& tdb) {
    BoolVarArgs none(0);
    for (int t=0; t!=nr_t; t++) {
        // count row
        int row_sum = nr_i;
        for (int i=0; i!=nr_i; i++)
            row_sum -= tdb[t][i];
        BoolVarArgs row(row_sum);
        // make row
        for (int i=0; i!=nr_i; i++) {
            if (1-tdb[t][i])
                row[--row_sum] = items[i];
        }

        // coverage: the trans its complement has no supported items
        // !row_compl[1] AND !row_compl[2] ...  <=> t_k
        clause(*this, BOT_AND, none, row, transactions[t]);
    }
}
*/

/*
void Fmrdb_basic::print_items(BoolVarArray items, BoolVarArray transactions, vector<bool>classes,int nr_i, int nr_t) const {
    for (int i=0; i!=nr_i; i++) {
        if (items[i].val() == 1)
            fprintf(solfile, "%i ", i);
    }
    if (classes.size() == 0) {
	    // unsigned int supp = 0;
        for (int t=0; t!=nr_t; t++)
            supp += transactions[t].val();
        fprintf(solfile, "(%i)\n", supp);

    } else { // labels are used
        int pos = 0; int neg = 0;
        for (int t=0; t!=nr_t; t++) {
            if (classes[t])
                pos += transactions[t].val();
            else
                neg += transactions[t].val();
        }
        fprintf(solfile, "(%i:+%i-%i)\n", (pos+neg), pos, neg);
    }
}
*/


/************************Constraints**********************************/

void Fmrdb_basic:: min_size_constraint(const BoolVarArgs& var, int size) {
    if (size <= 0)
        return;

    linear(*this, var, IRT_GQ, size);
}

///funtion for the degree  A-<R>-mindegree-B   input=B, output=A, normal is true if tbd=AxB and false if tbd=BxA
void Fmrdb_basic::degree_min_constraint(const BoolVarArgs& input, const vector< vector<bool> >& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal, int mindeg) {
    if (normal) {
        if (input.size() != nr_i ||
            output.size() != nr_t ||
            tdb.size() != (unsigned)nr_t)
            throw Exception("degree_min_constraint error", "dimensions don't match");

        IntArgs row(nr_i);
        for (int t=0; t!=nr_t; t++) {
            // make col_2
            for (int i=0; i!=nr_i; i++)
                row[i] = tdb[t][i];

            BoolVar aux(*this, 0, 1); // auxiliary variable
            linear(*this, row, input, IRT_GQ, mindeg,aux);
            // Aa => aux :: Aa =< aux
            rel(*this, output[t], IRT_LQ, aux);
        }
    } else {
        if (input.size() != nr_t ||
            output.size() != nr_i ||
            tdb.size() != (unsigned)nr_t)
            throw Exception("degree_min_constraint error", "dimensions don't match");

        IntArgs col(nr_t);
        for (int i=0; i!=nr_i; i++) {
            for (int t=0; t!=nr_t; t++)
                col[t] = tdb[t][i];

            BoolVar aux(*this, 0, 1); // auxiliary variable
            linear(*this, col, input, IRT_GQ, mindeg,aux);
            // Aa => aux :: Aa =< aux
            rel(*this, output[i], IRT_LQ, aux);
        }
    }
}

///funtion for the maximal degree  A-<R>-[mindegree]-B   input=B, output=A, normal is true if tbd=AxB and false if tbd=BxA

void Fmrdb_basic::maximal_degree_min_constraint(const BoolVarArgs& input, const vector<vector<bool> >& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal, int mindeg) {
    if (normal) {
        if (input.size() != nr_i ||
            output.size() != nr_t ||
            tdb.size() != (unsigned)nr_t)
            throw Exception("maximal_degree_min_constraint error", "dimensions don't match");

        IntArgs row(nr_i);
        for (int t=0; t!=nr_t; t++) {
            // make col_2
            for (int i=0; i!=nr_i; i++)
                row[i] = tdb[t][i];

            linear(*this, row, input, IRT_GQ, mindeg, output[t]);
        }
    } else {
        if (input.size() != nr_t ||
            output.size() != nr_i ||
            tdb.size() != (unsigned)nr_t)
            throw Exception("maximal_degree_min_constraint error", "dimensions don't match");

        IntArgs col(nr_t);
        for (int i=0; i!=nr_i; i++) {
            // make col_2
            for (int t=0; t!=nr_t; t++)
                col[t] = tdb[t][i];

            linear(*this, col, input, IRT_GQ, mindeg, output[i]);
        }
    }
}

void Fmrdb_basic::coverage_constraint(const Relation& rel, const vector<BitVec>& tdb, int e, const BoolVarArgs& target)
{
  int e1 = rel.ent1_id;
  int e2 = rel.ent2_id;
  if (e1 == e) {
    // we are rows (first dimension), coverage: e1==<e1 x e2>--e2 (input=e2, output=e1)
    //coverage_constraint(entities[e2], tdb, rel.ent1_size, rel.ent2_size, target, true);
    coverage_global(*this, tdb, entities[e2], target);
  } else {
    // we are cols (second dimension), coverage: e1--<e1 x e2>==e2 (input=e1, output=e2)
    //coverage_constraint(entities[e1], tdb, rel.ent1_size, rel.ent2_size, target, false);
    coverage_global(*this, tdb, entities[e1], target, true);
  }
}

/// funtion for the coverage I=<R>-O  input=I, output=T, tbd=TxI
//  if tdb=TxI, set normal=false
void Fmrdb_basic::coverage_constraint(const BoolVarArgs& input, const vector<BitVec>& tdb, int nr_t, int nr_i, const BoolVarArgs& output, bool normal) {
    if (normal) { // input=I, output=T, tdb=TxI
        if (input.size() != nr_i ||
            output.size() != nr_t ||
            tdb.size() != (unsigned)nr_t) {
            cout << "input.size()="<<input.size()<<" nr_i="<<nr_i<<"\n";
            cout << "output.size()="<<output.size()<<" nr_t="<<nr_t<<"\n";
            cout << "tdb.size()="<<tdb.size()<<" nr_t="<<nr_t<<"\n";
            throw Exception("coverage_constraint error", "dimensions don't match");
        }

        IntArgs row(nr_i);
        for (int t=0; t!=nr_t; t++) {
            // make 1-row
            for (int i=0; i!=nr_i; i++)
                row[i] = (1-tdb[t][i]);

            // coverage: the trans its complement has no supported items
            // sum((1-row(trans_s))*Items) = 0 <=> trans_s
            linear(*this, row, input, IRT_EQ, 0, output[t]);
        }
    } else { // input=I, output=T, tdb=IxT
        if (input.size() != nr_t ||
            output.size() != nr_i ||
            tdb.size() != (unsigned)nr_t) {
            cout << "input.size()="<<input.size()<<" nr_t="<<nr_t<<"\n";
            cout << "output.size()="<<output.size()<<" nr_i="<<nr_i<<"\n";
            cout << "tdb.size()="<<tdb.size()<<" nr_t="<<nr_t<<"\n";
            throw Exception("coverage_constraint error", "dimensions don't match");
        }

        IntArgs col(nr_t);
        for (int i=0; i!=nr_i; i++) {
            // make 1-col
            for (int t=0; t!=nr_t; t++)
                col[t] = (1-tdb[t][i]);

            // coverage: the trans its complement has no supported items
            // sum((1-row(trans_s))*Items) = 0 <=> trans_s
            linear(*this, col, input, IRT_EQ, 0, output[i]);
        }
    }
}


/// Print solution
void Fmrdb_basic::print(std::ostream& os) const {
    if (print_itemsets == PRINT_NONE) {
        return;
    } else if (print_itemsets == PRINT_FIMI) {
        // concise style (non-FIMI actually)
        for (int i=0;i<(int)entities.size();i++) {
            //fprintf(solfile, "\tentity[%i] ", i);
	    os << "\tentity[" << i << "]";
            int cnt = 0;
            for (int j=0;j<entities[i].size();j++)
                cnt += entities[i][j].val();
            //fprintf(solfile, " # %i", cnt);
	    os << " # " << cnt;
        }
        //fprintf(solfile, " -- obj=%f \n", obj.max());
	os << " -- obj=" << obj.max() << endl;
    } else if (print_itemsets == PRINT_FULL) {
        if (obj.max() != 0)
            //fprintf(solfile, "%f ", obj.max());
	    os << obj.max() << " ";
      
        const char* name;
        for (int e=0; e!=(int)entities.size(); e++) {
            //fprintf(solfile, "\tentity[%i] = %s", e, ent_names[e]);
            const char* ent_name = reader->ent_names[e];
            for (int j=0;j<entities[e].size();j++) {
                if (entities[e][j].val() == 1) {
                    // TODO Achille, something with (reverse) correspondence...?
                    name = reader->corresponding_names[e][j];
                    //fprintf(solfile, "%s.%s ", ent_name, name.c_str()); // offset 1 data
		    os << ent_name << "." << name << " " ;
                }
            }
            //fprintf(solfile, " # %i\n", cnt);
        }
        //fprintf(solfile, "\n");
	os << endl;
    } else if (print_itemsets == PRINT_CPVARS) {
        // Output CP variables (mostly for GIST)
        for (size_t i=0;i<entities.size();i++) {
            os << "\tentity["<<i<<"] (offset 0) = " << entities[i] << std::endl;
        }
        os << obj << std::endl;
    }
}

vector< vector<int> > Fmrdb_basic::get_pattern()
{
  vector< vector<int> > patt(entities.size());
  for (size_t e=0;e!=entities.size();e++) {
    for (int i=0;i<entities[e].size();i++) {
      if (entities[e][i].val() == 1)
        patt[e].push_back( i );
    }
  }
  return patt;
}


BoolVarArgs sort_by_freq(const BoolVarArray& items, const vector< vector<bool> >& tdb, int nr_i) {
  // get freqs
  vector< pair<int, int> > freqs(nr_i);
  for (int i=0; i!=nr_i; i++) {
    int freq = 0;
    for (size_t t=0; t!=tdb.size(); t++) {
      if (tdb[t][i])
        freq++;
    }
    freqs[i] = std::make_pair(freq, i);
  }
  
  std::stable_sort(freqs.begin(), freqs.end());
  
  BoolVarArgs sorted;
  for (int i=0; i!=nr_i; i++) {
    sorted << items[freqs[i].second];
  }
  return sorted;
}


void add_path(int node, const vector< vector<int> >& neigh, set< vector<int> >& paths, vector<int> prefix) {
  prefix.push_back(node);
  
  const vector<int>& neigh_node = neigh[node];
  // set prev to previous node (or 'node' if none already added)
  int prev = node;
  if (prefix.size() >= 2) // we already added 'node' so +1
    prev = prefix[prefix.size()-2];
    
  for (size_t i=0; i!=neigh_node.size(); i++) {
      int next = neigh_node[i];
      if (next != prev)
        add_path(next, neigh, paths, prefix);
  }
  
  if (prefix.size() >= 2) {
    // we need unique paths, but need order for 'prev' check
    // so sort here, AFTER recursing
    //std::sort(prefix.begin(), prefix.end());
    paths.insert( prefix );
  }
  return;
}

#endif
