/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <iostream>
#include "relation.hh"

using namespace std;


// row = ent1, col = ent2
vector<BitVec> Relation::to_tdb() const
{
    vector<BitVec> tdb(ent1_size, BitVec(ent2_size, 0));
    for(size_t i=0; i!=edges.size(); i++)
      tdb[edges[i].first][edges[i].second] = true;
    return tdb;
}

// row = ent2, col = ent1
vector<BitVec> Relation::to_vtdb() const
{
  vector<BitVec> tdb(ent2_size, BitVec(ent1_size, 0));
  for(size_t i=0; i!=edges.size(); i++)
    tdb[edges[i].second][edges[i].first] = true;
  return tdb;
}

void Relation::sort_yourself()
{
    vector< pair<double,size_t> > sort_order;
    sort_order.reserve(edge_weights.size());
    for(size_t i=0; i!=edge_weights.size(); i++) {
      double w = edge_weights[i];
      sort_order.push_back( make_pair(w, i) );
    }
    
    // sort weights and keep order
    sort(edge_weights.begin(), edge_weights.end(), std::greater<double>());
    sort(sort_order.begin(), sort_order.end(), std::greater< pair<double,size_t> >());
    
    // overwrite edges by sorted variant
    vector< pair<int,int> > edges_sorted;
    edges_sorted.reserve(edges.size());
    for(size_t i=0; i!=sort_order.size(); i++)
      edges_sorted.push_back( edges[sort_order[i].second] );
    edges = edges_sorted;
}
  
void Relation::print() const
{
    cout << "Relation: "<<header<<"\n";
    cout << "Ent1: "<<ent1_id<<", max="<<ent1_size<<"\n";
    cout << "Ent2: "<<ent2_id<<", max="<<ent2_size<<"\n";
    if (edges.size() != edge_weights.size())
      cout << "WARNING: edges ("<<edges.size()<<") and edge_weights ("<<edge_weights.size()<<") of different size\n";
    for(size_t i=0; i!=edges.size(); i++) {
      double w = 0.0;
      if (edge_weights.size() > i)
        w = edge_weights[i];
      cout << i <<": "<<edges[i].first<<"<--"<<w<<"-->"<<edges[i].second<<"\n";
    }
}

double Relation::get_density() const
{
    return double(edges.size())/ent1_size/ent2_size;
}
