/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __RELATION_HH__
#define __RELATION_HH__

#include <vector>
using namespace std;

#include <boost/dynamic_bitset.hpp>
typedef boost::dynamic_bitset<> BitVec;


struct Relation {
  string header;
  int ent1_id;
  int ent1_size;
  int ent2_id;
  int ent2_size;
  
  std::vector< std::pair<int,int> > edges;
  std::vector< double > edge_weights;

  Relation(string header0, int ent1_id0, int ent2_id0) :
    header(header0), ent1_id(ent1_id0), ent1_size(0), ent2_id(ent2_id0), ent2_size(0),
    edges(), edge_weights() { }
  
  // row = ent1, col = ent2
  vector<BitVec> to_tdb() const;
  // row = ent2, col = ent1
  vector<BitVec> to_vtdb() const;
  
  // sort by weights, decreasing
  void sort_yourself();
  
  void print() const;

  double get_density() const;
};

#endif
