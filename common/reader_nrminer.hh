/*
 *  Main authors:
 *      Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Some code based N-RMiner's CSVReader.cpp (by Eirini Spyropoulou)
 * 
 *  Copyright:
 *      all authors
 *
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef __FIMCP_COMMON_READERNRMINER__
#define __FIMCP_COMMON_READERNRMINER__

#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <sstream>
#include <cstring>
#include <dirent.h>
#include <errno.h>

#include "common/relation.hh"

/**
 * Class to read dir with N-RMiner files
 */
class Reader_NRMiner {
public:
  vector<Relation> rels;
  map<string, int> ent_ids;
  vector<const char*> ent_names; // of the entity _types_
  vector<map<string, int> > correspondence;
  vector<vector<const char*>> corresponding_names; // of the entities
  
  Reader_NRMiner() : rels(), ent_ids(), correspondence() { };
  ~Reader_NRMiner() { };
  
  virtual void read (const char* filename1);
  virtual void print();

protected:
  vector<string> list_dir(const char* dirname);
  void read_relfile(const string& relfile);
};


#endif
