//
// constraint_coverglobal.hpp
//
// global 'cover' constraint (also cover_inv for closed)
// 
// Made by Tias Guns <tias.guns@cs.kuleuven.be>

#include <set>

#include "constraint_coverglobal.hpp"

using namespace Gecode; 
using namespace Int;
using namespace std; 



// constraint post function
void coverage_global(Gecode::Space& home,
                     const vector<BitVec>& tdb,
                     const Gecode::BoolVarArgs& items0,
                     const Gecode::BoolVarArgs& trans0,
                     bool transpose
) {
  if (home.failed()) return;
  int nr_i = items0.size();
  int nr_t = trans0.size();
  if (!transpose)
    assert(nr_t == (int)tdb.size());
  else
    assert(nr_i == (int)tdb.size());
  
  // make vertical data. This is meant to be global so a memory leak for now...
  BitVec** vdata = new BitVec*[nr_i];
  for (int i=0; i!=nr_i; i++)
    vdata[i] = new BitVec(nr_t);
  
  vector<int> sizes(nr_t, 0);
  // transpose-compatible loops
  for (size_t t=0; t!=tdb.size(); t++) {
    for (size_t i=0; i!=tdb[t].size(); i++) {
      if (tdb[t][i]) {
        if (!transpose) {
          vdata[i]->set(t);
          sizes[t]++;
        } else { // tranpose TDB
          vdata[t]->set(i);
          sizes[i]++;
        }
      }
    }
  }
  for (size_t t=0; t!=sizes.size(); t++) {
    //if (sizes[t] == 0) // never covered, unless all i are false.
    //  rel(home, trans0[t], IRT_EQ, false);
    if (sizes[t] == nr_i) // always covered
      rel(home, trans0[t], IRT_EQ, true);
  }
  if (home.failed()) return;
  
  ViewArray<BoolView> items(home, items0);
  ViewArray<BoolView> trans(home, trans0);
  
  GECODE_ES_FAIL((CoverGlobal<BoolView,BoolView>::post(home,vdata,items,trans)));
}


template <class VA, class VB>
ExecStatus CoverGlobal<VA,VB>::propagate(Space& home, const ModEventDelta& )
{
  if (home.failed())
    return home.ES_SUBSUMED(*this);
  
  BitVec trans_set_false_neg;
  trans_set_false_neg.resize(nr_t, -1); // all true = none to false
  bool do_set_true = false; // if an item assigned false, check unavoidable
  bool do_set_false = false; // if an item assigned true
  
  // update items_free TODO: advisor
  int end = -1;
  size_t i = items_free.find_first();
  while (i != BitVec::npos) {
    if (items[i].assigned()) {
      if (items[i].val() == 1) {
        trans_set_false_neg &= (*vdata[i]); // all not in vdata should be set false
        do_set_false = true;
      } else { // val() == 0
        do_set_true = true;
      }
      items_free[i] = false;
    } else
      end = i;
    i = items_free.find_next(i);
  }
  items.size(end+1); // no need for copy constructor to consider anything later
  
  
  // assign some trans to false
  if (do_set_false) {
    trans_set_false_neg.flip(); // now it is trans_set_false
    trans_set_false_neg &= trans_free; // only those still free // TODO: initialize it with trans_free?
    size_t t = trans_set_false_neg.find_first();
    while (t != BitVec::npos) {
      GECODE_ME_CHECK(trans[t].eq(home,false));
      trans_free[t] = false;
      t = trans_set_false_neg.find_next(t);
    }
  }
  
  // check if some trans must be set true
  if (do_set_true || items.size() == 0) {
    BitVec temp(trans_free); // free trans covered by all items
    size_t i = items_free.find_first();
    while (i != BitVec::npos) {
      temp &= (*vdata[i]); // covered by this item
      i = items_free.find_next(i);
    }
    
    // all those still in temp are unavoidable, set to true
    size_t t = temp.find_first();
    while (t != BitVec::npos) {
      GECODE_ME_CHECK(trans[t].eq(home,true));
      trans_free[t] = false;
      t = temp.find_next(t);
    }
  }
  

  // check if trans vars assigned
  // invariant: at least one in neg (=1-vdata[*][t]) is unassigned
  // otherwise, trans[t] would not be free by above propagation
  bool nofix = false;
  end = 0;
  size_t t = trans_free.find_first();
  while (t != BitVec::npos) {
    if (trans[t].assigned()) {
      if (trans[t].val() == 1) {
        
        // set all neg to 0
        size_t i = items_free.find_first();
        while (i != BitVec::npos) {
          if (not (*vdata[i])[t]) {
            GECODE_ME_CHECK(items[i].eq(home,false));
            // keep items_free[i] because of NOFIX
            nofix = true;
          }
          i = items_free.find_next(i);
        }
        trans_free[t] = false;
        
      } else { // trans[t].val() == 0
        
        // there must exist one neg that has value 1
        // if cnt(neg)=0: fail, =1: prop, >=2: nothing
        size_t last = BitVec::npos; // none so far
        size_t i = items_free.find_first();
        while (i != BitVec::npos) {
          if (not (*vdata[i])[t]) {
            if (last == BitVec::npos)
              last = i; // first neg
            else
              break; // second neg, nothing to prop
          }
          i = items_free.find_next(i);
        }
        if (i == BitVec::npos) {
          if (last == BitVec::npos) { // none found, fail
            return Gecode::ES_FAILED;
          } else {
            // one found, all other negs are 0, this one must be 1
            GECODE_ME_CHECK(items[last].eq(home,true));
            // keep items_free[last] because of NOFIX
            nofix = true;
            trans_free[t] = false;
          }
        } else
          end = t;
        
      }
    } else
      end = t;
    t = trans_free.find_next(t);
  }
  trans.size(end+1);
  
  if (nofix)
    return ES_NOFIX;
  
  return ES_FIX;
}
