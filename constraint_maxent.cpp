//
// global constraint to bound MaxEnt
// 
// Started by Tias Guns <tias.guns@cs.kuleuven.be>

#include <set>

#include "constraint_maxent.hpp"

using namespace Gecode; 
using namespace Int;
using namespace std; 

// for making linear incremental
struct ValCache {
  int     max_edges;
  size_t  i;
  double  val;
};

// post, constraint over relations
void constraint_maxent(Gecode::Space& home,
                       FloatVar& obj_,
                       vector<BoolVarArray>& entities_,
                       const vector<Relation>& rels,
                       const OptBound opt_bnd
) {
  if (home.failed()) return;
  
  Float::FloatView obj(obj_);
  vector< ViewArray<BoolView> > entities;
  for (size_t e=0; e!=entities_.size(); e++)
    entities.push_back( ViewArray<BoolView>(home,BoolVarArgs(entities_[e])) );
  
  double nbOfEdges = 0;
  double nbOfPossibleEdges = 0;
  for (size_t r=0; r!=rels.size(); r++) {
    //density *= rels[r].get_density();
    nbOfEdges += rels[r].edges.size();
    double tmp = rels[r].ent1_size;
    tmp *= rels[r].ent2_size; // over temp because this product can overflow an int
    nbOfPossibleEdges += tmp;
  }
  double density = nbOfEdges/nbOfPossibleEdges;
  cout << "density:"<<density<<"\n";

  double DL_mult = log((1-density)/density);
  double DL_add = 0;
  for (size_t e=0; e!=entities.size(); e++)
      DL_add += entities[e].size();
  DL_add *= log(1/(1-density));
  

  GECODE_ES_FAIL((ConsMaxEnt<BoolView>::post(home, obj, entities, rels, DL_mult, DL_add, opt_bnd)));
}


template <class VA>
ExecStatus ConsMaxEnt<VA>::propagate(Space& home, const ModEventDelta& )
{
  //cout << "Called\n";
  if (home.failed())
    return home.ES_SUBSUMED(*this);
  
  //Size per entity
  vector<int> size_assigned(entities.size(), 0);
  vector<int> size_compatible(entities.size(), 0);
  for (size_t e=0; e!=entities.size(); e++) {
    for (int n=0; n!=entities[e].size(); n++) {
      if (!entities[e][n].assigned())
	size_compatible[e]++;
      else if (entities[e][n].val() == 1)
        size_assigned[e]++;
    }
  }

  double val;
  switch(opt_bnd) {
    case BND_naieve_orig:
      val = prop_naieve_orig(size_assigned);
      break;
    case BND_naieve:
      val = prop_naieve(size_assigned, size_compatible);
      break;
    case BND_linear:
      val = prop_linear(size_assigned, size_compatible);
      break;
    case BND_full:
      val = prop_full(size_assigned, size_compatible);
      break;
    case BND_black:
      val = prop_black(size_assigned, size_compatible);
      break;
    case BND_blacky:
      val = prop_blacky(size_assigned, size_compatible, home);
      if (val == -3.14)
        return ES_NOFIX;
      break;
    case BND_look:
      val = prop_look(size_assigned, size_compatible, home, obj.min());
      break;
    default:
      cout << "Warning: in constraint_maxent: no bound selected...\n";
      val = 0;
  }
  
  // sample code to set UB
  GECODE_ME_CHECK(obj.lq(home, val ));
  
  return ES_FIX;
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_naieve(const vector<int>& size_assigned,
                                   const vector<int>& size_compatible)
{
  double val = 0;
  vector<int> degree1; // some memory opt: reuse
  vector<int> degree2; // some memory opt: reuse
  
  // most naieve bound: per relation, count all possible weights
  for (size_t r=0; r!=rels.size(); r++) {
    int e1 = rels[r].ent1_id;
    int e2 = rels[r].ent2_id;
  
    degree1.clear();
    degree2.clear();
    edges_degrees(r, &rels[r], degree1, degree2);
    
    // Computation of the maximum area
    // what if 0 extra of E1 and all of E2 or 0 extra of E2 and all of E1, take max 
    int max_edges = max(size_assigned[e1]*size_compatible[e2], size_assigned[e2]*size_compatible[e1]);
    
    if (degree2[0] <= degree1[0]) {
      max_edges = max(max_edges, get_max_edges_if1(size_assigned[e1], degree1, size_assigned[e2], degree2));
    } else { // degree1[0] < degree2[0]
      max_edges = max(max_edges, get_max_edges_if1(size_assigned[e2], degree2, size_assigned[e1], degree1));
    }       
    
    // Compute contributions to val, max_edges biggest edges
    size_t i = rels_free[r].find_first();
    while(--max_edges >= 0) {
      val += rels[r].edge_weights[i];
      i = rels_free[r].find_next(i);
    }
  }
  val += base_val;
  //cout << "Computed nominator: "<<val<<"\n";
  
  
  // denominator... need minimum nr entities (lb=all assigned to 1)
  int min_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++)
    min_ents += size_assigned[e];

  double DL = DL_mult*min_ents + DL_add;
  //cout << "Computed denominator: "<<DL<<"\n";
  
  
  return (val/DL);
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_naieve_orig(const vector<int>& size_assigned)
{
  double val = 0;
  
  // most naieve bound: per relation, count all possible weights
  for (size_t r=0; r!=rels.size(); r++) {
    const Relation* rel = &rels[r];
    int e1 = rel->ent1_id;
    int e2 = rel->ent2_id;
    size_t i = rels_free[r].find_first();
    while(i != BitVec::npos) {
      int val1 = -1; int val2 = -1; // both unassigned
      if (entities[e1][ rel->edges[i].first ].assigned())
        val1 = entities[e1][ rel->edges[i].first ].val();
      if (entities[e2][ rel->edges[i].second ].assigned())
        val2 = entities[e2][ rel->edges[i].second ].val();
      
      if ( val1 == 0 || val2 == 0) {
        // is 0, ignore edge from now on
        rels_free[r][i] = false;
      } else if ( val1 == 1 && val2 == 1) {
        // both 1, add to base_val and ignore edge from now on
        base_val += rel->edge_weights[i];;
        rels_free[r][i] = false;
      } else {
        val += rel->edge_weights[i];
      }
      i = rels_free[r].find_next(i);
    }
  }
  val += base_val;
  //cout << "Computed nominator: "<<val<<"\n";
  
  // denominator... need minimum nr entities (lb=all assigned to 1)
  int min_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++)
    min_ents += size_assigned[e];
  double DL = DL_mult*min_ents + DL_add;
  //cout << "Computed denominator: "<<DL<<"\n";
  
  return (val/DL);
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_linear(const vector<int>& size_assigned,
                                   const vector<int>& size_compatible)
{
  // min/max nr entities
  int min_ents = 0;
  int max_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++) {
    min_ents += size_assigned[e];
    max_ents += size_compatible[e];
  }
  
  //if (max_ents == 0)
  //  return prop_naieve_orig(size_assigned);
  
  // pass for all relations over all edges and compute degrees
  vector< pair< vector<int>, vector<int> > > rel_degrees(rels.size());
  for (size_t r=0; r!=rels.size(); r++) {
    edges_degrees(r, &rels[r], rel_degrees[r].first, rel_degrees[r].second);
  }
  
  // compute maxval for early stopping (doing rel_val here is not more efficient)
  vector<int> rel_maxedges(rels.size());
  for (size_t r=0; r!=rels.size(); r++) {
    int e1 = rels[r].ent1_id;
    int e2 = rels[r].ent2_id;
    vector<int>& degree1 = rel_degrees[r].first;
    vector<int>& degree2 = rel_degrees[r].second;
    
    // Computation of the maximum area
    // what if 0 extra of E1 and all of E2 or 0 extra of E2 and all of E1, take max 
    int max_edges = max(size_assigned[e1]*size_compatible[e2], size_assigned[e2]*size_compatible[e1]);
    
    if (degree2[0] <= degree1[0]) {
      max_edges = max(max_edges, get_max_edges_if1(size_assigned[e1], degree1, size_assigned[e2], degree2));
    } else { // degree1[0] < degree2[0]
      max_edges = max(max_edges, get_max_edges_if1(size_assigned[e2], degree2, size_assigned[e1], degree1));
    }       
        
    rel_maxedges[r] = max_edges;
  }  
  
  double best = 0.0;
  // valcache to do incremental counting of vals
  ValCache init = {0, 0, 0.0};
  vector<ValCache> rel_valcache(rels.size(), init); // ValCache{ max_edges; i; val }
  // linear: try all entity counts from 0..max_ents
  for (int nr_ent = 0; nr_ent <= max_ents; nr_ent++) {
    double totval = 0.0;
    for (size_t r=0; r!=rels.size(); r++) {
      double val = 0.0;
      
      ValCache& prev = rel_valcache[r];
      if (prev.max_edges == rel_maxedges[r]) {
        val = prev.val;
      } else {
        int e1 = rels[r].ent1_id;
        int e2 = rels[r].ent2_id;
        vector<int>& degree1 = rel_degrees[r].first;
        vector<int>& degree2 = rel_degrees[r].second;
        
        // Computation of the maximum area, CAPPED
        // what if 0 extra of E1 and all of E2 or 0 extra of E2 and all of E1, take max 
        int max_edges = max(size_assigned[e1]*min(nr_ent,size_compatible[e2]), size_assigned[e2]*min(nr_ent,size_compatible[e1]));
        
        if (degree2[0] <= degree1[0]) {
          max_edges = max(max_edges, get_max_edges_if1(size_assigned[e1], degree1, size_assigned[e2], degree2, nr_ent));
        } else { // degree1[0] < degree2[0]
          max_edges = max(max_edges, get_max_edges_if1(size_assigned[e2], degree2, size_assigned[e1], degree1, nr_ent));
        }
        
        if (max_edges == prev.max_edges) {
          // nothing to do
          val = prev.val;
        } else {
          int max_edges_orig = max_edges; // copy
          size_t i = 0;
          const Relation& rel = rels[r];
          const BitVec& rel_free = rels_free[r];
          
          if (prev.max_edges == 0 || max_edges < prev.max_edges) {
            // must recompute from scratch (or first run)
            i = rel_free.find_first();
          } else {
            // continue from previous max_edges
            val = prev.val;
            max_edges -= prev.max_edges; // that many already counted
            i = prev.i; // previous 'find_next()'
          }
          
          while(--max_edges >= 0) {
            val += rel.edge_weights[i];
            i = rel_free.find_next(i);
          }
          // store
          prev.val = val;
          prev.i = i;
          prev.max_edges = max_edges_orig;
        }
      }
      
      totval += val;
    }
    totval += base_val;
    //cout << "Computed nominator: "<<val<<"\n";
    
    // denominator...
    double DL = DL_mult*(min_ents+nr_ent) + DL_add;
    //cout << "Computed denominator: "<<DL<<"\n";
    best = max(best, (totval/DL));
    //cout << "Linear["<<nr_ent<<"]: "<<totval<<"/"<<DL<<" = "<<(totval/DL)<<" (best="<<best<<") (max val="<<maxval<<")\n";
    
    // stop if val is maxval
    bool all_maxval = true;
    for (size_t r=0; r!=rels.size(); r++) {
      if (rel_valcache[r].max_edges != rel_maxedges[r]) {
        all_maxval = false;
        break;
      }
    } 
    if (all_maxval)
      break; // val will stay same, while DL will continue to decrease
  }
  
  return best;
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_full(const vector<int>& size_assigned,
                                 const vector<int>& size_compatible)
{
  int min_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++) {
    min_ents += size_assigned[e];
  }

  // pass for all relations over all edges and compute degrees
  vector< pair< vector<int>, vector<int> > > rel_degrees(rels.size());
  for (size_t r=0; r!=rels.size(); r++) {
    edges_degrees(r, &rels[r], rel_degrees[r].first, rel_degrees[r].second);
  }
  
  // precompute the 'val' values for all possible max_edges starting 0
  vector< vector<double> > rel_val(rels.size());
  for (size_t r = 0; r < rels.size(); r++) {
    double val = 0.0;
    rel_val[r].push_back( val ); // 0 edges = 0.0

    size_t i = rels_free[r].find_first();
    while(i != BitVec::npos) {
      val += rels[r].edge_weights[i];
      rel_val[r].push_back( val );
      i = rels_free[r].find_next(i);
    }
  }

  double best = 0.0;
  //Vector containing the size n_e of every entity e to consider. We loop
  //over every possible vector n_add, as entities_.size() nested for
  //loops, the inside loop being the one over n_add[0].
  vector<int> n_add(entities.size(),0);
  int sum_n_add = 0;
  while (true) {
    double val = 0.0;
    for (size_t r = 0; r < rels.size(); r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;
      
      int max_edges = n_add[e1]*n_add[e2]+n_add[e1]*size_assigned[e2]+n_add[e2]*size_assigned[e1];
      val += rel_val[r][max_edges];
    }
    val += base_val;

    //Denominator
    double DL = DL_add+DL_mult*(min_ents+sum_n_add);

    //Update best
    best = max(best,val/DL);

    //Find the next combination n_add to try, or break if there are none
    //e : the entity for which we increment n_add. For e' < e, n_add[e']
    //is set to 0 and for e' > e, n_add[e'] doesn't change.
    size_t e = 0;
    n_add[e] ++;
    sum_n_add ++;
    while ((val == 0 && n_add[e] != 1) || // val 0 and n_add[e] was 0: jump to next e
           (n_add[e] > size_compatible[e]) ||
           !is_a_valid_combination(&n_add, &rel_degrees, e) ) { 
      e++;
      if (e < entities.size()) {
	sum_n_add += 1 - n_add[e-1];
	n_add[e] ++;
      	n_add[e-1] = 0;
      } else {
	break;
      }
    }
    if (e >= entities.size())
      break;
  }
  return best;
}

// Checks whether n_add is a valid combination according to the degrees.
// We just look at the relations involving the entity e.
template <class VA>
forceinline bool ConsMaxEnt<VA>::is_a_valid_combination(const vector<int>* n_add,
                     const vector<pair<vector<int>, vector<int> > >* degree, size_t e)
{
  for (size_t r = 0; r < rels.size(); r++) {
    size_t e1 = rels[r].ent1_id;
    size_t e2 = rels[r].ent2_id;
    if (e1 == e || e2 == e) {
      if (n_add->at(e2) > 0 && n_add->at(e1) > degree->at(r).second[n_add->at(e2)-1]) {
	return false;
      } else if (n_add->at(e1) > 0 && n_add->at(e2) > degree->at(r).first[n_add->at(e1)-1]) {
        return false;
      }
    }
  }
  return true;
}


// Pass over all edges and compute the degrees in the projected database
template <class VA>
forceinline void ConsMaxEnt<VA>::edges_degrees(int r, const Relation* rel,
                                               vector<int>& degree1, vector<int>& degree2)
{
  assert(degree1.size() == 0); assert(degree2.size() == 0);
  
  degree1.resize(rel->ent1_size, 0);
  degree2.resize(rel->ent2_size, 0); 
  int e1 = rel->ent1_id;
  int e2 = rel->ent2_id;
  
  size_t i = rels_free[r].find_first();
  while(i != BitVec::npos) {  
    int val1 = -1; int val2 = -1; // both unassigned
    if (entities[e1][ rel->edges[i].first ].assigned())
      val1 = entities[e1][ rel->edges[i].first ].val();
    if (entities[e2][ rel->edges[i].second ].assigned())
      val2 = entities[e2][ rel->edges[i].second ].val();
    
    if (val1 == -1 && val2 == -1) {
      degree1[rel->edges[i].first] ++;
      degree2[rel->edges[i].second] ++;
    } else if ( val1 == 0 || val2 == 0) {
      // is 0, ignore edge from now on
      rels_free[r][i] = false;
    } else if ( val1 == 1 && val2 == 1) {
      // both 1, add to base_val and ignore edge from now on
      base_val += rel->edge_weights[i];;
      rels_free[r][i] = false;
    }
    i = rels_free[r].find_next(i);
  }
  
  sort(degree1.begin(), degree1.end(), greater<int>());
  sort(degree2.begin(), degree2.end(), greater<int>());
}

// max nr edges if at least 1 of e1 or e2
template <class VA>
forceinline int ConsMaxEnt<VA>::get_max_edges_if1(int size_assigned1, const vector<int>& degree1,
                                                  int size_assigned2, const vector<int>& degree2,
                                                  int cap
                                                 )
{
  int max_edges = 0;
  if (cap == -1) { // NOT capped
    
    // try E1 from 1..max(degree(E2))
    for (int e1_add = 1; e1_add <= degree2[0]; e1_add++) {
      int e2_add_max = degree1[e1_add-1]; // if e1_add edges, at most E2 as many as e1_add' biggest degree (-1 because offset 0)
      if (e2_add_max <= 0)
        break; // 0 other edges not checked here
      // check back that e2_add_max of E2 can lead to e1_add of E1, using degree2
      while (e2_add_max > 1 && degree2[e2_add_max-1] < e1_add)
        e2_add_max--;
      max_edges = max(max_edges, size_assigned1*e2_add_max + size_assigned2*e1_add + e1_add*e2_add_max);
    }
  } else { // CAPPED
    
    // try E1 from 1..max(degree(E2)) (or nr_ent-1; -1 cus nr_ent means other 0)
    for (int e1_add = 1; e1_add <= min(cap-1,degree2[0]); e1_add++) {
      int e2_add_max = min(cap-e1_add,degree1[e1_add-1]); // if e1_add edges, at most E2 as many as e1_add' biggest degree (-1 because offset 0)
      if (e2_add_max <= 0)
        break; // 0 other edges not checked here
      // check back that e2_add_max of E2 can lead to e1_add of E1, using degree2
      while (e2_add_max > 1 && degree2[e2_add_max-1] < e1_add)
        e2_add_max--;
      max_edges = max(max_edges, size_assigned1*e2_add_max + size_assigned2*e1_add + e1_add*e2_add_max);
    }
  }
  return max_edges;
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_black(const vector<int>& size_assigned,
  const vector<int>& size_compatible)
{
  int min_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++) {
    min_ents += size_assigned[e];
  }
  
  // pass for all relations over all edges and compute degrees
  vector< pair< vector<int>, vector<int> > > rel_degrees(rels.size());
  /*for (size_t r=0; r!=rels.size(); r++) {
   *   edges_degrees(r, &rels[r], rel_degrees[r].first, rel_degrees[r].second);
}*/
  
  // precompute the 'val' values for all possible nb_edges starting 0
  //
  // grey1 is the vector of values from one node assigned of entity e1
  // to one node unassigned of entity e2
  //
  // grey2 from a node assigned of ent e2 to a node unassigned of ent e1
  //
  // black between two nodes unassigned
  
  vector< vector<double> > rel_grey1_val(rels.size());
  vector< vector<double> > rel_grey2_val(rels.size());
  vector< vector<double> > rel_black_val(rels.size());
  for (size_t r = 0; r < rels.size(); r++) {
    double grey1_val = 0.0;
    double grey2_val = 0.0;
    double black_val = 0.0;
    
    rel_degrees[r].first.resize(rels[r].ent1_size, 0);
    rel_degrees[r].second.resize(rels[r].ent2_size, 0); 
    int e1 = rels[r].ent1_id;
    int e2 = rels[r].ent2_id;
    rel_grey1_val[r].push_back(grey1_val); // 0 edges = 0.0
    rel_grey2_val[r].push_back(grey2_val);
    rel_black_val[r].push_back(black_val);
    
    size_t i = rels_free[r].find_first();
    while(i != BitVec::npos) {
      int val1 = -1; int val2 = -1; // both unassigned
      if (entities[e1][ rels[r].edges[i].first ].assigned())
        val1 = entities[e1][ rels[r].edges[i].first ].val();
      if (entities[e2][ rels[r].edges[i].second ].assigned())
        val2 = entities[e2][ rels[r].edges[i].second ].val();
      
      if (val1 == -1 && val2 == -1) {
        rel_degrees[r].first[rels[r].edges[i].first] ++;
        rel_degrees[r].second[rels[r].edges[i].second] ++;
      } else if ( val1 == 0 || val2 == 0) {
        // is 0, ignore edge from now on
        rels_free[r][i] = false;
      } else if ( val1 == 1 && val2 == 1) {
        // both 1, add to base_val and ignore edge from now on
        base_val += rels[r].edge_weights[i];;
        rels_free[r][i] = false;
      }
      
      
      if (val1 == 1 && val2 == -1) {
        grey1_val += rels[r].edge_weights[i];
        rel_grey1_val[r].push_back(grey1_val);
      } else if (val1 == -1 && val2 == 1) {
        grey2_val += rels[r].edge_weights[i];
        rel_grey2_val[r].push_back(grey2_val);
      } else if (val1 == -1 && val2 == -1) {
        black_val += rels[r].edge_weights[i];
        rel_black_val[r].push_back(black_val);
      }
      i = rels_free[r].find_next(i);
    }
    
    sort(rel_degrees[r].first.begin(), rel_degrees[r].first.end(), greater<int>());
    sort(rel_degrees[r].second.begin(), rel_degrees[r].second.end(), greater<int>());
  }
  
  
  double best = 0.0;
  //Vector containing the size n_e of every entity e to consider. We loop
  //over every possible vector n_add, as entities_.size() nested for
  //loops, the inside loop being the one over n_add[0].
  vector<int> n_add(entities.size(),0);
  int sum_n_add = 0;
  while (true) {
    double val = 0.0;
    for (size_t r = 0; r < rels.size(); r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;
      
      int grey1_edges = size_assigned[e1]*n_add[e2];
      int grey2_edges = size_assigned[e2]*n_add[e1];
      int black_edges = n_add[e1]*n_add[e2];
      val += rel_grey1_val[r][grey1_edges];
      val += rel_grey2_val[r][grey2_edges];
      val += rel_black_val[r][black_edges];
    }
    val += base_val;
    
    //Denominator
    double DL = DL_add+DL_mult*(min_ents+sum_n_add);
    
    //Update best
    best = max(best,val/DL);
    
    //Find the next combination n_add to try, or break if there are none
    //e : the entity for which we increment n_add. For e' < e, n_add[e']
    //is set to 0 and for e' > e, n_add[e'] doesn't change.
    size_t e = 0;
    n_add[e] ++;
    sum_n_add ++;
    while ((val == 0 && n_add[e] != 1) || // val 0 and n_add[e] was 0: jump to next e
      (n_add[e] > size_compatible[e]) ||
      !is_a_valid_combination(&n_add, &rel_degrees, e) ) { 
      e++;
    if (e < entities.size()) {
      sum_n_add += 1 - n_add[e-1];
      n_add[e] ++;
      n_add[e-1] = 0;
    } else {
      break;
    }
      }
      if (e >= entities.size())
        break;
  }
  
  return best;
}
template <class VA>
forceinline double ConsMaxEnt<VA>::prop_blacky(const vector<int>& size_assigned,
                                               const vector<int>& size_compatible,
                                               Space& home
                                              )
{
  size_t nr_e = entities.size();
  size_t nr_r = rels.size();
  
  int min_ents = 0;
  for (size_t e = 0; e != nr_e; e++) {
    min_ents += size_assigned[e];
  }

  // pass for all relations over all edges and compute degrees
  vector< pair< vector<int>, vector<int> > > rel_degrees(nr_r);
  vector< pair< vector<int>, vector<int> > > rel_degrees_sorted(nr_r);
  /*for (size_t r=0; r!=rels.size(); r++) {
    edges_degrees(r, &rels[r], rel_degrees[r].first, rel_degrees[r].second);
  }*/
  
  // precompute the 'val' values for all possible nb_edges starting 0
  //
  // grey1 is the vector of values from one node assigned of entity e1
  // to one node unassigned of entity e2
  //
  // grey2 from a node assigned of ent e2 to a node unassigned of ent e1
  //
  // black between two nodes unassigned

  vector< vector<double> > rel_grey1_val(nr_r);
  vector< vector<double> > rel_grey2_val(nr_r);
  vector< vector<double> > rel_black_val(nr_r);
  for (size_t r = 0; r < nr_r; r++) {
    BitVec& rels_free_r = rels_free[r];
    double grey1_val = 0.0;
    double grey2_val = 0.0;
    double black_val = 0.0;

    rel_degrees[r].first.resize(rels[r].ent1_size, 0);
    rel_degrees[r].second.resize(rels[r].ent2_size, 0); 
    int e1 = rels[r].ent1_id;
    int e2 = rels[r].ent2_id;
    rel_grey1_val[r].push_back(grey1_val); // 0 edges = 0.0
    rel_grey2_val[r].push_back(grey2_val);
    rel_black_val[r].push_back(black_val);

    size_t i = rels_free_r.find_first();
    while(i != BitVec::npos) {
      int val1 = -1; int val2 = -1; // both unassigned
      if (entities[e1][ rels[r].edges[i].first ].assigned())
        val1 = entities[e1][ rels[r].edges[i].first ].val();
      if (entities[e2][ rels[r].edges[i].second ].assigned())
        val2 = entities[e2][ rels[r].edges[i].second ].val();

      if (val1 == -1 && val2 == -1) {
        rel_degrees[r].first[rels[r].edges[i].first] ++;
        rel_degrees[r].second[rels[r].edges[i].second] ++;
      } else if ( val1 == 0 || val2 == 0) {
        // is 0, ignore edge from now on
        rels_free_r[i] = false;
      } else if ( val1 == 1 && val2 == 1) {
        // both 1, add to base_val and ignore edge from now on
        base_val += rels[r].edge_weights[i];;
        rels_free_r[i] = false;
      }

	
      if (val1 == 1 && val2 == -1) {
	grey1_val += rels[r].edge_weights[i];
	rel_grey1_val[r].push_back(grey1_val);
      } else if (val1 == -1 && val2 == 1) {
	grey2_val += rels[r].edge_weights[i];
	rel_grey2_val[r].push_back(grey2_val);
      } else if (val1 == -1 && val2 == -1) {
	black_val += rels[r].edge_weights[i];
	rel_black_val[r].push_back(black_val);
      }
      i = rels_free_r.find_next(i);
    }
    
    rel_degrees_sorted[r] = rel_degrees[r];
    sort(rel_degrees_sorted[r].first.begin(), rel_degrees_sorted[r].first.end(), greater<int>());
    sort(rel_degrees_sorted[r].second.begin(), rel_degrees_sorted[r].second.end(), greater<int>());
  }


  double best = 0.0;
  bool changed = false;
  vector< vector<double> > bestvals(nr_e); // [e][x]=val: best val for exactly 'x' entities of type 'e'
  for (size_t e=0; e!=nr_e; e++)
    bestvals[e].resize(size_compatible[e]+1, 0); // offset 0
  //Vector containing the size n_e of every entity e to consider. We loop
  //over every possible vector n_add, as entities_.size() nested for
  //loops, the inside loop being the one over n_add[0].
  vector<int> n_add(nr_e,0);
  int sum_n_add = 0;
  while (true) {
    double val = 0.0;
    for (size_t r = 0; r < nr_r; r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;
      
      int grey1_edges = size_assigned[e1]*n_add[e2];
      int grey2_edges = size_assigned[e2]*n_add[e1];
      int black_edges = n_add[e1]*n_add[e2];
      val += rel_grey1_val[r][grey1_edges];
      val += rel_grey2_val[r][grey2_edges];
      val += rel_black_val[r][black_edges];
    }
    val += base_val;

    //Denominator
    double DL = DL_add+DL_mult*(min_ents+sum_n_add);
    val = val/DL;
    
    //Update best
    best = max(best,val);

    // store best for exactly n_add edges of that type
    for (size_t e=0; e!=nr_e; e++) {
      double& v = bestvals[e][n_add[e]];
      if (v < val)
        v = val;
    }
    
    //Find the next combination n_add to try, or break if there are none
    //e : the entity for which we increment n_add. For e' < e, n_add[e']
    //is set to 0 and for e' > e, n_add[e'] doesn't change.
    size_t e = 0;
    n_add[e] ++;
    sum_n_add ++;
    while ((val == 0 && n_add[e] != 1) || // val 0 and n_add[e] was 0: jump to next e
           (n_add[e] > size_compatible[e]) ||
           !is_a_valid_combination(&n_add, &rel_degrees_sorted, e) ) { 
      e++;
      if (e < nr_e) {
	sum_n_add += 1 - n_add[e-1];
	n_add[e] ++;
      	n_add[e-1] = 0;
      } else {
	break;
      }
    }
    if (e >= nr_e)
      break;
  }
  
  //* do partial look-ahead using bestvals:
  // first, fetch minsize, all sizes (of this entity type) lower are below the threshold
  double minval = obj.min();
  if (base_val < minval) {
    vector<int> minsizes(nr_e, 0);
    for (size_t e=0; e!=nr_e; e++) {
      for (size_t x=0; x!=bestvals[e].size(); x++) {
        // if bigger than minval, this and all higher entity sizes can do better than minval
        if (bestvals[e][x] >= minval)
          break;
        minsizes[e]++;
      }
    }
    
    // second, check in rel_degrees which entities in relation have a degree lower than corresponding minsizes
    for (size_t r=0; r!=nr_r; r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;

      int minsize = minsizes[e2];
      ViewArray<VA>& ents1 = entities[e1];
      for (int x=0; x!=ents1.size(); x++) {
        if (rel_degrees[r].first[x] < minsize && !ents1[x].assigned()) {
	  GECODE_ME_CHECK(ents1[x].eq(home,false));
          changed = true;
        }
      }
      
      minsize = minsizes[e1];
      ViewArray<VA>& ents2 = entities[e2];
      for (int x=0; x!=ents2.size(); x++) {
        if (rel_degrees[r].second[x] < minsize && !ents2[x].assigned()) {
          GECODE_ME_CHECK(ents2[x].eq(home,false));
          changed = true;
        }
      }
    }
  }

  if (changed)
    return -3.14; // hack to say that NOFIX (should rerun this propagator)
  return best;
}

template <class VA>
forceinline double ConsMaxEnt<VA>::prop_look(const vector<int>& size_assigned,
                                             const vector<int>& size_compatible,
                                             Space& home,
                                             double minval)
{
  // make bitvec of compatible entities
  vector<BitVec> comp_entities;
  comp_entities.reserve(size_compatible.size());
  for (size_t e=0; e!=entities.size(); e++) {
    comp_entities.push_back( BitVec(entities[e].size()) );
    BitVec& bv = comp_entities[e];
    for (int n=0; n!=entities[e].size(); n++) {
      if (!entities[e][n].assigned())
        bv[n] = 1;
    }
  }
  
  int min_ents = 0;
  for (size_t e = 0; e != size_assigned.size(); e++) {
    min_ents += size_assigned[e];
  }


  vector<BitVec> proj_entities(size_compatible.size());
  // update rel_free
  for (size_t r = 0; r != rels.size(); r++) {
    const Relation* rel = &rels[r];
    size_t e1 = rel->ent1_id;
    size_t e2 = rel->ent2_id;
    
    size_t i = rels_free[r].find_first();
    while(i != BitVec::npos) {  
      int val1 = -1; int val2 = -1; // both unassigned
      if (!comp_entities[e1][ rel->edges[i].first ])
        val1 = entities[e1][ rel->edges[i].first ].val();
      if (!comp_entities[e2][ rel->edges[i].second ])
        val2 = entities[e2][ rel->edges[i].second ].val();
      
      //if (val1 == -1 && val2 == -1) {
      //  degree1[rel->edges[i].first] ++;
      //  degree2[rel->edges[i].second] ++;
      if ( val1 == 0 || val2 == 0) {
        // is 0, ignore edge from now on
        rels_free[r][i] = false;
      } else if ( val1 == 1 && val2 == 1) {
        // both 1, add to base_val and ignore edge from now on
        base_val += rel->edge_weights[i];
        rels_free[r][i] = false;
      }
      i = rels_free[r].find_next(i);
    }
  }
  double bestval = base_val/(DL_mult*min_ents+DL_add);
  
  // for each entity of each type
  // XXX: possible heuristic: only for entities with lowest degree?
  for (size_t e=0; e!=entities.size(); e++) {
    BitVec& bv = comp_entities[e];
    size_t n = bv.find_first();
    while (n != BitVec::npos) {
      
      //* restore projection state
      for (size_t en=0; en!=entities.size(); en++)
        proj_entities[en] = comp_entities[en];
      proj_entities[e][n] = 0; // disable me

      /*cout << "e : " << e << ", n : " << n << endl;
      cout << proj_entities[0] << endl;
      cout << proj_entities[1] << endl;
      cout << proj_entities[2] << endl;*/
        
      //* project relations, mark compatible entities
      double base_me = 0.0;
      for (size_t r = 0; r != rels.size(); r++) {
        size_t e1 = rels[r].ent1_id;
        size_t e2 = rels[r].ent2_id;
        if (e == e1 || e == e2) {
          bool first = (e == e1);
          size_t size = 0;
          if (first)
            size = rels[r].ent2_size;
          else
            size = rels[r].ent1_size;
          BitVec proj(size, 0); // entities that remain after projection

          
          size_t i = rels_free[r].find_first();
          while(i != BitVec::npos) {  
            if (first && (size_t)rels[r].edges[i].first == n) {
              // other entity is compatible
              proj[ rels[r].edges[i].second ] = 1;
              base_me += rels[r].edge_weights[i]; // because we now assume me=1
            } else if (!first && (size_t)rels[r].edges[i].second == n) {
              // other entity is compatible
              proj[ rels[r].edges[i].first ] = 1;
              base_me += rels[r].edge_weights[i]; // because we now assume me=1
            }
            i = rels_free[r].find_next(i);
          }
          // project other entity type
          if (first) {
            proj_entities[e2] &= proj;
	  } else {
            proj_entities[e1] &= proj;
	  }
        }
      }

      
      //* compute bound, copied from BND_naieve_orig
      double val = base_val + base_me;
      for (size_t r=0; r!=rels.size(); r++) {
        const Relation* rel = &rels[r];
        int e1 = rel->ent1_id;
        int e2 = rel->ent2_id;
        size_t i = rels_free[r].find_first();
        while(i != BitVec::npos) {
          if (proj_entities[e1][ rel->edges[i].first ] &&
              proj_entities[e2][ rel->edges[i].second ]) {
            val += rel->edge_weights[i];
          }
          i = rels_free[r].find_next(i);
        }
      }
      //cout << "Computed nominator: "<<val<<"\n";
      double DL = DL_mult*(min_ents+1) + DL_add;
      //cout << "Computed denominator: "<<DL<<"\n";
      double bnd = (val/DL);

      // look-ahead pruning, if bnd smaller, entity[e][n] is impossible
      if (bnd < minval) {
        //cout << "["<<e<<"]["<<n<<"] minval: " <<minval<<" bnd: "<<bnd<<" bestval:"<<bestval<<"\n";
        GECODE_ME_CHECK(entities[e][n].eq(home,false));
      }
        

      bestval = max(bestval, bnd);
      
      //* end of this look-ahead
      n = bv.find_next(n);
    }
  }
  return bestval;
}
