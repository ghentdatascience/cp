This is the software repository for the paper "Direct Mining of
Subjectively Interesting Relational Patterns" by Tias Guns, Achille Aknin, Jefrey Lijffijt and Tijl De Bie.
See icdm16_direct_subjective.pdf

Installation
============
Download gecode-4.4.0 en unpack it somewhere
in that directory, run:
  ./configure && make

Config your terminal with (change 'somewhere' to the right directory!):
  export GECODE_HOME=/somewhere/gecode-4.4.0
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$GECODE_HOME

Then make this software:
  make


Usage
=====
Standard usage is as follows:

./rel_maxent -datafile data/sample/

(see data description below)

The program has a lot of options, mostly from gecode and their use does not matter. run ./rel_maxent -help to see them all.
Those that matter are:
-datafile   directory with datasets to use
-minent     minimum maximum entropy score
-iter       number of iterations of finding the best pattern and updating the distribution, default=1 (=0 infinitely)

also potentially relevant:
-bnd        bound to use (naieve, naieve2, linear, full, black, blacky, look) 
-noempty    no empty entities (0=allow some empty, 1=no empty ents)
-solfile    filename to write solutions to (any name)
-output     what to print, just nr per entity type (normal) or actual entities (full)

For example:
./rel_maxent -datafile data/sample2 -iter 5 -output full


Data format
===========
For each relation (between 2 entities) you need 2 files, for example:
rel.txt
rel_values.txt

rel.txt example:
e1,e2
1,1
1,3
2,1

the first line is the names of the two entities (in order), and all subsequent rows lists which entities are connected.

rel_values.txt example:
e1,e2
1.1975
0.213943
0.556206

it has to start with the same names of the two entities, followed by the log score of that edge according to the MaxEnt distribution. The _values files can be automatically generated. The code for this is in the 'get_values' directory. Run it for example like this:
./get_values/get_values data/sample2


to create a multi-relational dataset, use the same entity name in different relation files.

See example directories 'data/sample/' and 'data/sample2/' for sample datasets with 1 and 2 relations.
