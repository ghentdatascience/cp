/*
 *  Main authors:
 *    Tias Guns <tias.guns@cs.kuleuven.be>
 *
 *  Copyright:
 *      Tias Guns, 2016
 *
 *
 *  This file was part of Fim_cp, Frequent Itemset Mining using
 *  Constraint Programming, and uses Gecode.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software and associated documentation files (the
 *  "Software"), to deal in the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <algorithm>
#include <set>
#include <map>

#include "common/fmrdb_basic.hh"
#include "common/constraint_coverglobal.hpp"

void Fmrdb_basic::run(const Options_maxent& opt) {

  common_construction(opt);
  const vector<Relation>& rels = reader->rels;
  int num_entities = entities.size();
  
  // entity centric view
  vector< vector<int> > entity_relids(num_entities);
  for (size_t r=0; r!=rels.size(); r++) {
    entity_relids[rels[r].ent1_id].push_back( r );
    entity_relids[rels[r].ent2_id].push_back( r );
  }
  // rel to tdb
  // TODO: avoid this
  vector< vector<BitVec> > rel_tdbs;
  for (size_t r=0; r!=rels.size(); r++)
    rel_tdbs.push_back( rels[r].to_tdb() );
  
  /*********************************************************************/
  // constraints
  // whether a type has an entity
  BoolVarArray type_hasent(*this, num_entities, 0, 1);
  for (int e = 0; e != num_entities; e++)
    rel(*this, BOT_OR, entities[e], type_hasent[e]);
  
  for (int e=0; e!=num_entities; e++) {
      // neigh_hasent and auxiliary coverage for 'all zero' filter:
      // if all neigh (and me) no entities, then ignore the coverage
      BoolVarArgs neigh_hasent;
      neigh_hasent << type_hasent[e];
      BoolVarArray aux_cover(*this, entities[e].size(), 0, 1);
    
      // new: join it all together!
      BoolVarArgs allItems;
      vector<BitVec> allvTdb;
      // to allow the 'no neighbors' case, extra fake item
      BoolVar ent_noneigh(*this, 0, 1);
      allItems << ent_noneigh;
      allvTdb.push_back( BitVec(entities[e].size(), 0) );
      for (size_t x=0; x!=entity_relids[e].size(); x++) {
        int relid = entity_relids[e][x];
        if (rels[relid].ent1_id == e) {
          // we are rows (first dimension), so append vtdb
          allItems << entities[rels[relid].ent2_id];
          neigh_hasent << type_hasent[rels[relid].ent2_id];
          const vector<BitVec>& vtdb = rels[relid].to_vtdb();
          allvTdb.insert(allvTdb.end(), vtdb.begin(), vtdb.end());
        } else { // if (rels[relid].ent2_id == e) {
          // we are cols (second dimension), so tdb is vtdb for us
          allItems << entities[rels[relid].ent1_id];
          neigh_hasent << type_hasent[rels[relid].ent1_id];
          const vector<BitVec>& vtdb = rels[relid].to_tdb();
          allvTdb.insert(allvTdb.end(), vtdb.begin(), vtdb.end());
        }
      }
      
      // link neighbors to ent_noneigh: ent_noneigh <-> not type_hasent[e] /\ not type_hasent[e_x] /\ ...
      BoolVarArgs pos;
      BoolVarArgs neg = neigh_hasent;
      clause(*this, BOT_AND, pos, neg, ent_noneigh);
      coverage_global(*this, allvTdb, allItems, entities[e], true);
  }
  
  // no empty sets (automatically connected)
  if (opt.noempty() == 1) {
    // TODO: we should ideally add cardinality constraints 'reified' (see old degree_min_constraint())
    for (int e = 0; e != num_entities; e++)
      rel(*this, BOT_OR, entities[e], 1);
  
    
  } else {
    linear(*this, type_hasent, IRT_GQ, 2); // at least two entity types
    
    // enforce connectedness across entity types
    set< vector<int> > paths;
    vector< vector<int> > neighbors(num_entities);
    for (int e = 0; e != num_entities; e++) {
      for (size_t x=0; x!=entity_relids[e].size(); x++) {
        int relid = entity_relids[e][x];
        if (rels[relid].ent1_id == e)
          neighbors[e].push_back(rels[relid].ent2_id);
        else
          neighbors[e].push_back(rels[relid].ent1_id);
      }
    }
    
    for (int e = 0; e != num_entities-1; e++) // -1 because path[0] < path[-1] below
      add_path(e, neighbors, paths);
    
    set< vector<int> >::iterator it;
    for (it = paths.begin(); it != paths.end(); ++it)
    {
      const vector<int>& path = *it;
      size_t path_size = path.size();
      if (path_size > 2 && path[0] < path[path_size-1]) {
        // if the first and last ents of the path have entities
        // so must all entities in between
        // path[0] && path[-1] -> path[1:-1]
        // == path[1:-1] \/ !path[0] \/ !path[-1]
        BoolVarArgs pos;
        if (path_size == 3) {
          pos << type_hasent[path[1]];
        } else {
          BoolVar aux(*this, 0, 1);
          BoolVarArgs midents;
          for (size_t i=0; i!= path_size-1; i++)
            midents << type_hasent[path[i]];
          rel(*this, BOT_AND, midents, aux);
          pos << aux; // aux <-> and(path[1:-1])
          
          // TODO: check if other path with same start/stop,
          // if so, add its aux <-> and(midents) to pos as well!
          set< vector<int> >::iterator otherit;
          for (otherit = paths.begin(); otherit != paths.end(); ++otherit) {
            const vector<int>& otherpath = *otherit;
            if (otherit != it && otherpath.size() == path_size &&
                otherpath[0] == path[0] && otherpath[path_size-1] == path[path_size-1]) {
              cout << "Multiple paths between "<<path[0]<<" and "<<path[path_size-1]<<"...\n";
              BoolVar otheraux(*this, 0, 1);
              BoolVarArgs othermidents;
              for (size_t i=0; i!= path_size-1; i++)
                othermidents << type_hasent[otherpath[i]];
              rel(*this, BOT_AND, othermidents, otheraux);
              pos << otheraux; // otheraux <-> and(otherpath[1:-1])
            }
          }
        }
        BoolVarArgs neg;
        neg << type_hasent[path[0]] << type_hasent[path[path_size-1]];
        clause(*this, BOT_OR, pos, neg, 1);
      }
    }
  }
  
  /**********************************************************************/
  // objective function
  
  // no objective function here
  do_opt = false;
  obj = FloatVar(*this, 0.0, 0.0);
  

  /**********************************************************************/
  // search
  Branchings branching = (Branchings)opt.branching();
  if (branching == B_AUTO)
    branching = B_JOINT_INVDEGREE_MAX;
  
  if (branching == B_INPUT) {
    // We make an array with the variables of all entities joined
    BoolVarArgs join;
    for (int e = 0; e != num_entities; e++)
      join << entities[e];
    
    branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
  } else {
    // init degrees per entity type
    vector< vector<int> > ent_degrees(entities.size());\
    for (int e = 0; e != num_entities; e++)
      ent_degrees[e].resize(entities[e].size(), 0);
    
    // get degrees
    for (size_t r=0; r!=rels.size(); r++) {
      int e1 = rels[r].ent1_id;
      int e2 = rels[r].ent2_id;
      for (size_t i=0; i!=rels[r].edges.size(); i++) {
        ent_degrees[e1][rels[r].edges[i].first]++;
        ent_degrees[e2][rels[r].edges[i].second]++;
      }
    }
    
    if (branching != B_JOINT_DEGREE_MIN) {
      // substract full count of other in rel (so it becomes complement of degree)
      vector<int> maxdeg(num_entities, 0);
      for (size_t r=0; r!=rels.size(); r++) {
        maxdeg[rels[r].ent1_id] += rels[r].ent2_size;
        maxdeg[rels[r].ent2_id] += rels[r].ent1_size;
      }
      for (int e = 0; e != num_entities; e++) {
        int s = maxdeg[e];
        for (int x=0; x!=entities[e].size(); x++)
          ent_degrees[e][x] = ent_degrees[e][x]-s;
      }
    }
    
    // joint branchers
    if (branching == B_JOINT_DEGREE_MIN || branching == B_JOINT_INVDEGREE_MAX) {
      vector< pair<int, pair<int,int> > > indices;
      for (int e = 0; e != num_entities; e++) {
        indices.reserve(indices.size()+entities[e].size());
        for (int n=0; n!=entities[e].size(); n++) {
          pair<int,int> p (e,n);
          indices.push_back( make_pair(ent_degrees[e][n],p));
        }
      }
      std::sort(indices.begin(), indices.end());
      
      BoolVarArgs join;
      for (size_t i=0; i!=indices.size(); i++)
        join << entities[indices[i].second.first][indices[i].second.second];
      branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
    }
    
    // by entity branchers
    if (branching == B_MINSIZE_INVDEG) {
      // first from small to large entity size
      vector< pair<int,int> > ent_indices; ent_indices.reserve(num_entities);
      for (int e = 0; e != num_entities; e++)
        ent_indices.push_back( make_pair(entities[e].size(), e) );
      std::sort(ent_indices.begin(), ent_indices.end());
      
      for (size_t i=0; i!=ent_indices.size(); i++) {
        int e = ent_indices[i].second;
        // then, from large to small ent_degree (either deg or invdeg)
        vector< pair<int,int> > indices;
        indices.reserve(entities[e].size());
        for (int n=0; n!=entities[e].size(); n++)
          indices.push_back( make_pair(-ent_degrees[e][n],n));
        std::sort(indices.begin(), indices.end());
        
        BoolVarArgs join;
        for (size_t i=0; i!=indices.size(); i++)
          join << entities[e][indices[i].second];
        branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
      }
      
    } else if (branching == B_MAXREL_INVDEG) {
      // first from large to small nr relations
      vector<int> ent_rels(num_entities);
      for (size_t r=0; r!=rels.size(); r++) {
        ent_rels[rels[r].ent1_id]++;
        ent_rels[rels[r].ent2_id]++;
      }
      vector< pair<int,int> > ent_indices; ent_indices.reserve(num_entities);
      for (int e = 0; e != num_entities; e++)
        ent_indices.push_back( make_pair(-ent_rels[e], e) );
      std::sort(ent_indices.begin(), ent_indices.end());
      
      for (size_t i=0; i!=ent_indices.size(); i++) {
        int e = ent_indices[i].second;
        // then, from large to small ent_degree (either deg or invdeg)
        vector< pair<int,int> > indices;
        indices.reserve(entities[e].size());
        for (int n=0; n!=entities[e].size(); n++)
          indices.push_back( make_pair(-ent_degrees[e][n],n));
        std::sort(indices.begin(), indices.end());
        
        BoolVarArgs join;
        for (size_t i=0; i!=indices.size(); i++)
          join << entities[e][indices[i].second];
        branch(*this, join, INT_VAR_NONE(), INT_VAL_MAX());
      }
    }
  }
  
}

int main(int argc, char* argv[]) {
    Options_maxent opt(strchr(argv[0],'/')+1);
    opt.datafile("../data/main.txt");
    opt.description("This model finds relational frequent patterns, given 3 entities and 2 relations, with no additional constraints");
    opt.usage("-datafile somedir");
    opt.parse(argc, argv);
    Script::run<Fmrdb_basic,DFS,Options_maxent>(opt);

   return 0;
}
